class Card:
    def __init__(self, name, hp=0, attackpoints=0, attackspeed=0):
        self.name = name
        self.hp = hp
        self.attackpoints = attackpoints
        self.attackspeed = attackspeed
        self.isDead = False

    def getName(self):
        return self.name

    def getAttackpoints(self):
        return self.attackpoints

    def getAttackspeed(self):
        return self.attackspeed

    def getHp(self):
        return self.hp

    def isDead(self):
        print(self.getName() + ' is dead')
        self.isDead = True

    def setName(self, name):
        self.name = name

    def setAttackpoints(self, attackpoints):
        self.attackpoints = attackpoints

    def setAttackSpeed(self, attackspeed):
        self.attackspeed = attackspeed

    def setHp(self, hp):
        self.hp = hp

    def setEquipment(self, equipment):
        self.attackpoints += equipment.getAttackpoints()
        self.attackspeed += equipment.getAttackspeed()
        self.hp += equipment.getHp()

    def Attack(self, victim):
        if victim.hp > 0:
            victim.hp -= self.attackpoints * self.attackspeed

        if victim.hp <= 0:
            victim.hp = 0
            victim.isDead = True
