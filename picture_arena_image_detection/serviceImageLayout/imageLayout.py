import math

import requests

from PIL import Image, ImageDraw, ImageFont

equipement = {  "hp": 2,
        "attack": 2,
        "speedattack": 2,
        "value": 2
        }


card =  {
        "id": 8,
        "urlImg": "https://5eti-bucket.s3.amazonaws.com/animal/cat3.png",
        "owner": 1,
        "idRef": 1,
        "name": "cat",
        "rarete": "SECRETSUPERRARE",
        "type": "ANIMAL",
        "hp": 1,
        "attack": 10,
        "speedattack": 100,
        "value": 1000
    }


historique = {
    "type": "HISTORIQUE",
    "hp": 10,
    "attack": 10,
    "speedattack": 100,
    "value": 1000

}

def downloadImage(urlImg,filename):
    with open(filename, 'wb') as file:
        response = requests.get(urlImg, stream=True)
        if not response.ok:
            print(response)
        file.write(response.content)
    file.close()


#downloadImage(card["urlImg"],"pic1.jpg")

def concatOnImage(imagePath,rankPath):
    background = Image.open('pic1.jpg')
    foreground = Image.open("../static/images/ssr.png")

    background = Image.alpha_composite(
        Image.new("RGBA", background.size),
        background.convert('RGBA')
    )

    background.paste(
        foreground,
        (0, 0),
        foreground
    )
    #background.show()
    return background


def get_concat_v(im1, im2):
    imgs = [im1,im2]

    min_img_width = min(i.width for i in imgs)

    total_height = 0
    for i, img in enumerate(imgs):
        if img.width > min_img_width:
            imgs[i] = img.resize((min_img_width, int(img.height / img.width * min_img_width)), Image.ANTIALIAS)
        total_height += imgs[i].height

    img_merge = Image.new(imgs[0].mode, (min_img_width, total_height))
    y = 0
    for img in imgs:
        img_merge.paste(img, (0, y))

        y += img.height
    rgb_im = img_merge.convert('RGB')

    return rgb_im

def setCardInfos(background,card_dic,equip_dic=None):
    #background =concat1(1,1)
    #background.show()
    img = Image.new('RGB', (background.size[0], 25), color='#FFFFFF')

    # get a font

    # get a drawing context
    canvas = ImageDraw.Draw(img)
    attack = card_dic["attack"]
    hp = card_dic["hp"]
    speed = card_dic["speedattack"]
    value = card_dic["value"]

    if( equip_dic is not None):
        attack +=  equip_dic["attack"]
        hp +=  equip_dic["hp"]
        speed += equip_dic["speedattack"]
        value += equip_dic["value"]

    # draw text, half opacity
    #TODO set attack hp..properties
    canvas.text((0, 5),  "attack:"+str(attack) +
                " hp:" +str(hp) +
                 " speed:"+str(speed) +
                 " price:"+str(value),  fill=(0, 0, 0, 128))
    #img.show()
    return get_concat_v(background, img)


#add transparent layerout to extend image size
def preConcatVlayoutEmpty(image):
    foreground = image.convert('RGBA')
    width, height = foreground.size


    width_added = math.floor(0.2*width)
    height_added =  math.floor(0.1*height)
    background = Image.open("./static/images/layer_empty.png")

    background = background.resize((width+width_added,height+height_added))

    background = Image.alpha_composite(
        Image.new("RGBA", background.size),
        background.convert('RGBA')
    )

    background.paste(
        foreground,
        (math.floor(width_added/2), math.floor(height_added/4)),
        foreground
    )

    return background

#add rank_image as  layerout
def concatVlayout(image,image_rank):
    #foreground = Image.open('pic3.jpg').convert("RGBA")
    #background = Image.open("./images/back.png")

    background = image.convert("RGBA") #image originale
    foreground = image_rank # layout

    width, height = background.size
    foreground = foreground.resize((width,height))

    background = Image.alpha_composite(
        Image.new("RGBA", background.size),
        background.convert('RGBA')
    )

    background.paste(
        foreground,
        (0, 0),
        foreground
    )

    #background.show()
    return background

def FinalImageVequipement(card_dic,equip_dic=None):
    im = Image.open(requests.get(card_dic["urlImg"], stream=True).raw);
    im_with_infos =setCardInfos(im,card_dic  )
    #TODO image_rank en fonction de rank
    image_rank = Image.open("./static/images/back.png")
    final_image = concatVlayout(im_with_infos,image_rank)
    final_image.show()
    return final_image

#add attack and hp value on image at rigth position
def addInfosOnImage(image,dic,equip_dic=None):

    width, height = image.size

    # get a drawing context
    canvas = ImageDraw.Draw(image)
    attack = dic["attack"]
    hp = dic["hp"]

    if (equip_dic is not None):
        attack += equip_dic["attack"]
        hp += equip_dic["hp"]

    # draw text, half opacity
    font_size=math.floor( min(width,height)/10)

    width_rate_attack = width*(0.83)
    width_rate_hp = width * (0.08)
    height_rate_attack =height*(0.85)
    height_rate_hp =height * (0.87)

    font = ImageFont.truetype("./static/font/arial.ttf", size=font_size)
    #font = ImageFont.load_default()
    attack_position = (width_rate_attack,height_rate_attack)
    canvas.text(attack_position,  str(attack), fill=(0, 0, 0, 0),font=font)

    hp_position =(width_rate_hp ,height_rate_hp)
    canvas.text(hp_position,  str(hp), fill=(0, 0, 0, 0),font = font)

    canvas.text((width*(0.06),height*(0.01)), dic["name"], fill=(0, 0, 0, 0),font = font)
    # img.show()
    return image


def FinalImage(image,category,label,card_dic):

    if category is None or label is None :
        print("category or label is None")
        return None
    category = category.lower()

    image = preConcatVlayoutEmpty(image)
    rank = SelectRankImage(card_dic["rarete"])
    if(category =='historique' ):
        #TODO image en fonction de rarete
        #image_rank = Image.open("./static/images/back.png")
        image_rank = Image.open("./static/images/"+rank)
        final_image = concatVlayout(image, image_rank)

    elif(category=="animal" or category =="equipement"):
        #image_rank = Image.open("./static/images/back.png")
        image_rank = Image.open("./static/images/"+rank)
        final_image = concatVlayout(image, image_rank)

    final_image= addInfosOnImage(final_image,card_dic)
    return final_image



def SelectRankImage(rank):
    dict={"COMMON":"backCommon.png","SECRETSUPERRARE":"backSR.png","RARE":"backR.png"}
    rank = rank.upper()
    return dict[rank]
#concat1(1,1)
#setCardInfos()
#concatVlayout(1,1)
#returnFinalImage(card)

