import base64
import os
from subprocess import PIPE, run
from geopy.geocoders import Nominatim

parent_path = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
darknet_path = os.path.join(parent_path, "darknet", "build", "darknet", "x64")
darknet_path_data = os.path.join(darknet_path, "data")

'''
#use cmd
def imageDetection(fileName):
    os.chdir(darknet_path)
    command = "darknet detect cfg/yolov3.cfg yolov3.weights data/"+fileName
    result = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True, shell=True)
    return result.stdout


my_output = imageDetection("dog.jpg")
'''

# os.system("darknet detect cfg/yolov3.cfg yolov3.weights data/dog.jpg")
# initialize Nominatim API
'''
geolocator = Nominatim(user_agent="gogo")

Latitude = "45.7838219"
Longitude = "4.8687568"

location = geolocator.reverse(Latitude + "," + Longitude)

# Display
print(location)
'''

import requests

def testUpload(category):
    url = "http://localhost:5000/upload/image"
    #  first, encode our image with base64

    if category=="historique":
        image_file="lyon.jpg"
    elif category=="animal":
        image_file="cat.jpg"


    with open(image_file, "rb") as f:
        im_bytes = f.read()
        f.close()
    im_b64 = base64.b64encode(im_bytes).decode("utf8")
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

    payload = {"image": im_b64, "category":category,"userId":1,"longitude":4.8687568,"latitude":45.7838219}
    response = requests.post(url, json=payload, headers=headers)



testUpload("animal")

