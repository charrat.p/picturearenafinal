import base64
import io

from serviceTraitement.categoryCfg import animal_category, equipement_category, historique_category
from serviceTraitement.S3 import uploadS3
from serviceTraitement.imageGeoInfo import getGPS
from PIL import Image


def base2image(im_b64):
    # convert it into bytes
    img_bytes = base64.b64decode(im_b64.encode('utf-8'))
    # convert bytes data to PIL Image object
    img = Image.open(io.BytesIO(img_bytes))
    return img

# determiner la categorie dectectee de l'image apres yolo
def treatDetectedImage(upload_category, dic):
    if not dic:  # si dic est vide, donc rien n'est detecte
        return upload_category, None

    sorted_dic = sorted(dic.items(), key=lambda kv: kv[1],reverse=True)
    upload_category = upload_category.lower()
    for label, prob in sorted_dic:
        if label in animal_category and upload_category == "animal":
            return upload_category, label
        elif label in equipement_category and upload_category == "equipement":
            return upload_category, label
    return upload_category, None


#TODO Ameliorer la selection A changer
#si l'image ne contient pas les metadonnes GPS,alos on doit fournir longitude et latitude
def treatHistorique(file_name,longitude=None,latitude=None):
    seuil = 0.2
    dic = getGPS(file_name)
    label = None

    #si l'image ne contient pas les metadonnes GPS
    if not dic:
        lon=longitude
        lat=latitude
        if(longitude is None and latitude is None):
            print("fournir longitude et latitude! L'image ne les contient pas")
            return None
    else:
        lon=dic["longitude"]
        lat=dic["latitude"]

    print("long lat:"+str(lon)+" "+str(lat))
    for historique in historique_category:
        if abs(historique["longitude"] - lon <= seuil) \
                and abs(historique["latitude"] - lat <= seuil):
            label = historique["personnage"]
    print("treatHist label:"+label)
    return label

#treatHistorique('lyon.jpg')

