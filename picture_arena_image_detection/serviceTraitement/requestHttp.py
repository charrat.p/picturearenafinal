
import requests
from PIL import Image

from serviceTraitement.S3 import uploadS3
from serviceImageLayout.imageLayout import FinalImage

CardService_url = "http://204.236.239.109:80"
UserService_url = "http://54.152.148.26:80"



def getCardTemplate(label):
    url = CardService_url+"/cardref/"+label
    headers = {'Content-type': 'application/json'}
    r = requests.get(url,headers=headers)
    return r.json()

def postCard(userId,label):
    url = CardService_url+"/card"
    headers = {'Content-type': 'application/json'}
    params = {"userId":userId,"label":label}
    r = requests.post(url,headers=headers,params=params)
    print(r.text)

def test():
    headers = {'Content-type': 'application/json'}
    # create and get cardId
    try:
        params = {"cardId": 11, "urlImageWL": "hhttptttept///"}
        requests.post(CardService_url+"/addcardbrut", headers=headers, params=params)
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        print("create card error")




def request2UserService(user_id):
    param = {"userId": user_id}
    try:
        r = requests.get('http://httpbin.org/post', data=param)
        print(r.text)
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        raise SystemExit(e)
    return r.text


def createCard(filename_temp,userId,category,label,username,dateString=None):
    url = CardService_url
    headers = {'Content-type': 'application/json'}
    params = {"userId":userId,"label":label}
    #create and get cardId
    if(checkUserPermission(dateString,userId)=="false"):return False
    try:
        cardId = requests.post(url+"/card",headers=headers,params=params).text
        cardId = int(cardId)
        print("cardID = " +str(cardId))
        cards2User(cardId,username)
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        print("create card error")
  
    #upload to AWS  S3
    uploadname =str(userId)+"_"+str(cardId)+".png"
    print("uploadname= " + uploadname)
    urlImageWL = uploadS3(filename_temp,category,uploadname)
    #update urlImageWL

    try:
        params = {"cardId":cardId,"urlImageWL":urlImageWL.split("?")[0]}
        requests.post(url+"/addcardbrut",headers=headers,params=params)
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        print("update card error")
        return False


    card_dic= getCardTemplate(label)

    #create image with layout
    imageVL = FinalImage(Image.open(filename_temp), category, label,card_dic)

    imageVL.show()
    filename_temp = filename_temp.split(".")[0]+"_VL.png"
    imageVL.save(filename_temp)
    uploadname = str(userId) + "_" + str(cardId) + "_"+"VL"+".png"
    urlImage = uploadS3(filename_temp, category, uploadname)
    #update urlImageWL
    try:
        params = {"cardId":cardId,"urlImage":urlImage.split("?")[0]}
        requests.post(url+"/addcardlayer",headers=headers,params=params)
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        print("update card error")
        return False

    return True


def checkUserPermission(dateString,userId):
    url = UserService_url+"/users/"+userId +"/time/"
    #  first, encode our image with base64
    #params = {"date": "2022-01-18 17:22:22"}
    params = {"date": dateString}
    response = requests.post(url, params=params)
    return response.text

def cards2User(cardId,username):
    url = UserService_url+"/users/"+username +"/cards"
    params = {"cardId": cardId}
    response = requests.post(url, params=params)
    return response.text
