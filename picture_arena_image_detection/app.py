import glob
import os
from os import abort
from random import randint
from PIL import Image

from flask import Flask, flash, request, redirect, url_for, send_from_directory
from yoloModule.yolo_detection import imageDetection,postfix_image
from serviceTraitement.cardImage import  treatHistorique, treatDetectedImage
from serviceTraitement.cardImage import base2image
from serviceTraitement.requestHttp import  createCard
#UPLOAD_FOLDER = darknet_path+"/data/"

UPLOAD_FOLDER="./"
ALLOWED_EXTENSIONS = { 'png', 'jpg'}

app = Flask(__name__)
app.secret_key = "super secret key"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename_temp):
    return '.' in filename_temp and \
           filename_temp.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# return redirect(url_for('download_file', name=filename_temp[:-4]+postfix_image))


@app.route('/download_file/<name>')
def download_file(name):
    return send_from_directory(app.config["UPLOAD_FOLDER"], name)

@app.route("/holla")
def holla():
    return "Senorita"

@app.route('/upload/image',methods=['POST'])
def add_image():
    print("Hello world")
    if request.method == 'POST':
        if not request.json or 'image' not in request.json:
            abort(400)

        deleteLocalImage()
        upload_category= request.json['category']
        userId= request.json['userId']
        img = base2image( request.json['image'])
        username= request.json['username']
        dateString = request.json['date']
        filename_temp = "upload_"+str(userId)+"_"+ str(randint(0,100))+str(randint(0,100)) +'.jpg'
        print("save filename_temp:", filename_temp)
        img.save(filename_temp)
        #detection des objets
        print(userId)
        if upload_category == "historique":
            longitude = float(request.json.get("longitude"))
            latitude = float(request.json.get("latitude"))
            category,label = "historique", treatHistorique(filename_temp,longitude,latitude)
            print("historique analyse: ",category,label)
        else:
            # detection des animaux/equipements/historique
            dic = imageDetection(image=filename_temp)
            category, label = treatDetectedImage(upload_category, dic)
            print("animal/equipement analyse: " ,category, label )

        if(label is None):
            return "False"

        #TODO creer carte avec layout et upload
        #ans = createCard(filename_temp, userId, category, label)
        ans = createCard(filename_temp, userId, category, label,username,dateString=dateString)



        if ans:
            return "True"
        else:
            return "False"

        return "True"


        #TODO verifier avec UserService:
        '''
        #verifier utilisateur a droit de creer une carte ou pas
        isCreateImage = request2UserService(userId)
        if isCreateImage.lower() == "true":
            ans = createCard(userId,upload_category,dic,filename_temp)
            return ans
        else:
            return "Dépasser la limite de création par jour"
        '''
        #print(uploadS3(filename_temp))
    #deleteLocalImage("upload_horse.jpg")



def deleteLocalImage():
    for file in glob.glob("upload*"):
        os.remove(file)
        pass

if __name__ == '__main__':
    #deleteLocalImage("upload_horse.jpg")

    app.debug = True
    app.run(host='0.0.0.0')
