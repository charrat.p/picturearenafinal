# Picture Arena

**PICTURE ARENA** is the project of : 

 - Philippe CHARRAT : [GitLab](gitlab.com/charrat.p)
 - Yongbin GUO : [GitLab](https://gitlab.com/guo-yong/)
 - Antoine CROCQUEVIELLE : [GitLab](https://gitlab.com/Antoine-Croc)
 -  Fares BOUAMAR

This was the final project of our CLBD major course at CPE Lyon for the year 2021/2022.

## GOAL OF APPLICATION
Mobile video game allowing players to collect cards, which they can acquire by taking pictures of them in real life.

Players will then be able to select a few and battle them to earn in-game currency and make their faction shine.
![enter image description here](https://philippecharrat.000webhostapp.com/logo_big.png)

## SPECIFICATIONS

These are defined here: https://docs.google.com/document/d/1HcuA5bFTdLgp8vEYQAO7Q60gHvDjUXdEhn_5lUZ5xpQ/edit
 

## DESCRIPTION 

### Mobile Application 
The application was developed with the Android Studio IDE with JAVA.
As an additional resource, we used three additional libraries:

 1. Picasso : Image Manager Librairie [(doc)](https://square.github.io/picasso/)
 2. Jackson : JSON Manager Librairie [(doc)](https://github.com/FasterXML/jackson)
 3. Socket-IO : Socket Manager to connect to the notification node server [(doc)](https://github.com/nkzawa/socket.io-android-chat)
 4. Volley : HTTP Librairie [(doc)](https://github.com/google/volley)

### GAME LOGIC 
The services have been designed as a micro-service. For time reasons, it was decided to convert them back to SOA service. 
A partial implementation of Bearer token authentication [(doc)](https://swagger.io/docs/specification/authentication/bearer-authentication/) has been implemented. However, services are exposed in configurations.

 - **Card** Service : Service composed of two parts. The template, the card with statistics, rarity and types (animal, historic, equipment). The model, the unique part with the players photo URL.
 - **Application** Service : Contains all the other elements of the game. We find the models of factions, arena and store. In addition, there is transaction to have a sales history and Challenge for fights.

### IMAGE DETECTION
Server in python to receive an image from the user. This will detect the object or animal present in the image.
A choice on our part is that the server is "god" on card creation. It will contact the various other services such as Users or Cards. He will also be in charge of creating the image of the card with the layer according to its rarity and depositing the two images on S3.

Additional resources:
 - **Flask** : Framework to create a server with Python [(doc)](https://flask.palletsprojects.com/en/2.0.x/)
 - **YOLO** : A model to detect objects in images [(here)](https://arxiv.org/abs/1506.02640)

### GAME MANAGER 
We chose to process combat asynchronously. A player will submit a challenge in an arena. A second player will be free to choose it and play it.
The architecture is a micro-service in Python with a */beginAMatch* endpoint.
The server will check the decklists before doing the fight.
It returns several elements:
 - A JSON : containing the steps of the match and the decks of the two players. The Json will be replayed by the view in the application with a Timer of one second.
 - Requests to Challenge Service to close the match and distribute factions and arena points and money.
 - Notification in Node Server to warn the second player of the result of his match.

Additional resources:
 - **Flask** : Framework to create a server with Python [(doc)](https://flask.palletsprojects.com/en/2.0.x/)

### NOTIFICATION SERVER 
A server that works with two channels. The first is a REST API to add a notification to one of the players. You must send your id and the result of the match.
This is stored in a HashMAP with the primary key player id.
During a connection of the application in web-socket, we send a socket containing all the pending results and reset the string.

Additional resources:

 - **Express** : Framework Node to make a web server [(doc)](https://expressjs.com/fr/starter/installing.html)
 - **Socket-IO** : Socket-IO: library to have sockettes to communicate with Android [(doc)](https://practicalprogramming.fr/socket-io)


### POI SERVER 
A web server to retrieve points of interest around the player with nearby lon/lat. If in its database, there are less than three points of interest, new ones are created.

 - Apache2 : An HTTP server [(doc)](https://doc.ubuntu-fr.org/apache2) 
 - PostgreSQL : Database
 - PostGIS : PostGIS is a spatial database extender for PostgreSQL object-relational database. [(doc)](https://postgis.net/)

