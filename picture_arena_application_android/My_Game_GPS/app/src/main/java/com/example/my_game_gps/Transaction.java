package com.example.my_game_gps;

import java.io.Serializable;

public class Transaction implements Serializable {
    private static final long serialVersionUID = 1L;
    private int idAcheteur;
    private int idCarte;
    private int idVendeur;

    public Transaction(int idAcheteur, int idCarte, int idVendeur) {
        this.idAcheteur = idAcheteur;
        this.idCarte = idCarte;
        this.idVendeur = idVendeur;
    }
}
