package com.example.my_game_gps;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.my_game_gps.databinding.FighterItemBinding;

import java.util.List;

public class FighterListAdapter  extends
        RecyclerView.Adapter<FighterListAdapter.ViewHolder> {
        List<Fighter> fighterList;
    private ListenerFighter listener;

    public FighterListAdapter(List<Fighter> fighterList) {
        assert fighterList != null;
        this.fighterList = fighterList;
    }

    public void setFighterList(List<Fighter> fighterList) {
        this.fighterList = fighterList;

    }

    @NonNull
    @Override
    public FighterListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FighterItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.fighter_item, parent,false);
        return new FighterListAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FighterListAdapter.ViewHolder holder, int position) {
        Fighter file = fighterList.get(position);
        holder.binding.title.setText(file.getName());
        holder.binding.faction.setText(file.getFaction());
        holder.binding.level.setText(file.getLevel());
        holder.binding.price.setText(file.getPrice());
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Listener",listener.toString());
                listener.acceptADuel(file.getId(),file.getPrice());
            }
        });
    }

    public void setListener(ListenerFighter listener) {  this.listener = listener; }

    @Override
    public int getItemCount() {
        return fighterList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private FighterItemBinding binding;
        ViewHolder(FighterItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
