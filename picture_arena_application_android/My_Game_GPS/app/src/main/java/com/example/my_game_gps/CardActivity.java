package com.example.my_game_gps;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class CardActivity extends AppCompatActivity {

    private int idPlayer;
    private int idCard;
    private String token;
    private CardActivity activity;

    private String title;
    private String urlImage;

    private Button backToDeck;
    private Button sellCard;

    private TextView cardName;
    private TextView cardHp;
    private TextView cardAttack;
    private TextView cardSpeedAttack;
    private TextView cardPrice;
    private ImageView cardView;

    private int attack;
    private int hp;
    private int price;
    private int speedattack;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);
        this.activity = this;
        Intent intent = getIntent();
        if (intent != null){
            idPlayer = intent.getIntExtra("idPlayer",0);
            idCard = intent.getIntExtra("idCard",0);
            if (intent.hasExtra("urlImage")){
                urlImage = intent.getStringExtra("urlImage");
            }
            if (intent.hasExtra("token")){
                token = intent.getStringExtra("token");
            }
            if (intent.hasExtra("title")){
                title = intent.getStringExtra("title");
            }

            attack  = intent.getIntExtra("attack",0);
            hp  = intent.getIntExtra("hp",0);
            speedattack  = intent.getIntExtra("speedattack",0);
            price  = intent.getIntExtra("price",0);
        }

        cardName = (TextView) this.findViewById(R.id.cardName);
        cardName.setText(title);

        cardAttack = (TextView) this.findViewById(R.id.attack_to_set);
        cardAttack.setText(String.valueOf(attack));

        cardHp = (TextView) this.findViewById(R.id.hp_to_set);
        cardHp.setText(String.valueOf(hp));

        cardSpeedAttack = (TextView) this.findViewById(R.id.speedattack_to_set);
        cardSpeedAttack.setText(String.valueOf(speedattack));

        cardPrice = (TextView) this.findViewById(R.id.price_to_set);
        cardPrice.setText(String.valueOf(price));

        cardView = (ImageView) this.findViewById(R.id.card_view);
        Picasso.get().load(urlImage).into(cardView);

        this.backToDeck = (Button) this.findViewById(R.id.back_to_deck);
        this.backToDeck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(1, intent);
                finish();
            }
        });

        this.sellCard = (Button) this.findViewById(R.id.sell_card);
        this.sellCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder myPopUp = new AlertDialog.Builder(activity);
                myPopUp.setTitle("sell your card");
                RequestQueue queue = Volley.newRequestQueue(activity);
                String message = "Do you want sell this card ?";
                myPopUp.setMessage(message);
                myPopUp.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //TODO call api
                        RequestQueue requestQueue = Volley.newRequestQueue(activity);
                        String URL = "http://54.152.148.26:80/shop/";
                        Log.i("TOKEN",token);

                        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.i("VOLLEY", response);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("VOLLEY", error.toString());
                            }
                        }) {
                            @Override
                            protected Map<String,String> getParams(){
                                Map<String,String> params = new HashMap<String, String>();
                                params.put("userId", String.valueOf(idPlayer));
                                params.put("cardId", String.valueOf(idCard));
                                params.put("urlImg", urlImage);
                                params.put("price", String.valueOf(price));
                                Log.d("Params",params.toString());
                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String,String> params = new HashMap<String, String>();
                                params.put("Content-Type","application/x-www-form-urlencoded");
                                params.put("Authorization", "Bearer "+token);
                                Log.d("Params",params.toString());
                                return params;
                            }

                            @Override
                            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                                String responseString = "";
                                if (response != null) {
                                    responseString = String.valueOf(response.statusCode);
                                    // can get more details such as response.headers
                                }
                                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                            }
                        };
                        requestQueue.add(stringRequest);

                    }
                });
                myPopUp.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                myPopUp.show();
            }
        });


    }
}