package com.example.my_game_gps;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.my_game_gps.databinding.CartesListFragementBinding;
import com.example.my_game_gps.databinding.FighterListFragementBinding;

import java.util.List;

public class FigtherListFragement extends Fragment {

    private FighterListFragementBinding binding;
    private FighterListAdapter figtherListAdapter;
    private ListenerFighter listener;
    
    private String token;
    private String idArena;
    private String idPlayer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,
                R.layout.fighter_list_fragement,container,false);
        binding.figtherList.setLayoutManager(new LinearLayoutManager(
                binding.getRoot().getContext()));

        if(figtherListAdapter != null) {
            binding.figtherList.setAdapter(figtherListAdapter);
            figtherListAdapter.setListener(this.listener);
        }
        binding.buttonGoCreateChallenge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),CreatChallengeActivity.class);
                intent.putExtra("token", token);
                intent.putExtra("idArena", idArena);
                intent.putExtra("idPlayer", idPlayer);
                startActivity(intent);
            }
        });
        return binding.getRoot();
    }

    public void setList(List<Fighter> fakeList) {
        if(binding != null) {
            binding.figtherList.setAdapter(new FighterListAdapter(fakeList));
        }
        else figtherListAdapter = new FighterListAdapter(fakeList);
    }

    public void setListener(ListenerFighter listener) {this.listener = listener;}

    public void setBoutton(String idPlayer, String idArena, String token, String factionCtrl) {
        this.idPlayer = idPlayer;
        this.idArena = idArena;
        this.token = token;
        binding.arenaCtrInput.setText(factionCtrl);
    }

    public void modifyCards(List<Fighter> fakeList, ListenerFighter listenerNew) {
        Log.e("CHANGE DATA","We change the fighters");
        figtherListAdapter.setFighterList(fakeList);
        figtherListAdapter.notifyDataSetChanged();
    }
}
