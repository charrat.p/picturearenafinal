package com.example.my_game_gps;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Inventory extends AppCompatActivity {

    private int idPlayer;
    private String username;
    private String faction;
    private String token;

    private Button buttonDeck;
    private Button buttonParametre;
    private Button buttonFaction;

    private TextView usernameText;
    private TextView factionText;
    private TextView money;
    private String moneyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);

        Intent intent = getIntent();
        if (intent != null) {
            idPlayer = intent.getIntExtra("idPlayer",0);
            if (intent.hasExtra("username")) {
                username = intent.getStringExtra("username");
            }
            if (intent.hasExtra("token")) {
                token = intent.getStringExtra("token");
            }
            if (intent.hasExtra("faction")) {
                faction = intent.getStringExtra("faction");
            }
            if (intent.hasExtra("money")) {
                moneyText = intent.getStringExtra("money");
            }

        }
        this.usernameText = (TextView) this.findViewById(R.id.usernameText);
        this.usernameText.setText(username);
        this.money = (TextView) this.findViewById(R.id.money);
        this.money.setText(moneyText);

        this.factionText = (TextView) this.findViewById(R.id.factionText);
        this.factionText.setText(faction);

        this.buttonDeck = (Button) this.findViewById(R.id.button_deck);
        this.buttonDeck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Inventory.this, DeckActivity.class);
                intent.putExtra("idPlayer", idPlayer);
                intent.putExtra("token",token);
                intent.putExtra("username",username);
                startActivity(intent);
            }
        });

        this.buttonFaction = (Button) this.findViewById(R.id.button_faction);
        this.buttonFaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Inventory.this, FactionActivity.class);
                intent.putExtra("idPlayer", idPlayer);
                intent.putExtra("token",token);
                intent.putExtra("faction",faction);
                startActivityForResult(intent,1);
            }
        });

        this.buttonParametre = (Button) this.findViewById(R.id.button_options);
        this.buttonParametre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Inventory.this, PreferencesActivity.class);
                intent.putExtra("idPlayer", idPlayer);
                intent.putExtra("token",token);
                intent.putExtra("faction",faction);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}