package com.example.my_game_gps;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.my_game_gps.databinding.ActivityShopBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShopActivity extends AppCompatActivity {

    /* -- Activity Adapter -- */
    private ActivityShopBinding binding;
    private CarteListFragement carte_list;
    private List<Carte> shopList;
    private ShopActivity activity;
    private ListenerCards listener;

    /* -- Player Attribute -- */
    private int idPlayer;
    private String token;

    /* -- Volley Attribute -- */
    private RequestQueue requestQueue;
    private String shopItemsString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_shop);
        this.activity = this;
        this.idPlayer = 0;
        this.requestQueue = Volley.newRequestQueue(activity);
        Intent intent = getIntent();
        if (intent != null) {
            idPlayer = intent.getIntExtra("idPlayer", 0);
            if (intent.hasExtra("token")) {
                token = intent.getStringExtra("token");
            }
        }
        showStartup();
    }

    public void showStartup() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        carte_list = new CarteListFragement();
        transaction.replace(R.id.fragment_container,carte_list);
        transaction.commit();
        shopList = new ArrayList<Carte>();
        carte_list.setList(shopList);
        listener = new ListenerCards(){
            @Override
            public void actionOnACard(Integer id,String name,Integer price, Integer idOwner, Carte card) {
                AlertDialog.Builder myPopUp = new AlertDialog.Builder(activity);
                myPopUp.setTitle("Buy a card");
                String message = "Confirmed confirm the purchase of the "+name+" card for "+price+" golds";
                myPopUp.setMessage(message);
                myPopUp.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.i("API REQUEST","Contact API");
                        String URL = "http://54.152.148.26:80/transactions/"+card.getIdShopItem();
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.i("VOLLEY", response);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("VOLLEY", error.toString());
                            }
                        }) {
                            @Override
                            protected Map<String,String> getParams(){
                                Map<String,String> params = new HashMap<String, String>();
                                params.put("idAcheteur", String.valueOf(idPlayer));
                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String,String> params = new HashMap<String, String>();
                                params.put("Content-Type","application/x-www-form-urlencoded");
                                params.put("Authorization", "Bearer "+token);
                                Log.d("Params",params.toString());
                                return params;
                            }

                            @Override
                            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                                String responseString = "";
                                if (response != null) {
                                    responseString = String.valueOf(response.statusCode);
                                    Toast toast = Toast.makeText(activity, "card buy", Toast.LENGTH_LONG);
                                    toast.show();
                                }
                                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                            }
                        };
                        requestQueue.add(stringRequest);
                    }
                });
                myPopUp.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        System.out.println("Ok, you give up");
                    }
                });
                myPopUp.show();

            }
        };
        carte_list.setListener(listener);
        publishTheShop();
    }

    public void publishTheShop() {

        String URL = "http://54.152.148.26:80/shop/";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("VOLLEY", response);
                shopItemsString = response;
                displayShop();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error is ", "" + error);
            }
        });
        requestQueue.add(stringRequest);

    }


    protected void displayShop() {
        try {
            JSONArray jsonArray = new JSONArray(shopItemsString);
            Log.i("JSON ARRAY", jsonArray.toString());
            for(int j=0; j < jsonArray.length();j++) {
                    JSONObject obj = jsonArray.getJSONObject(j);
                    Log.i("SHOP Convert", obj.toString());
                    shopList.add(new Carte(obj.getInt("cardId"),obj.getInt("userId"),"",obj.getInt("price"),obj.getString("urlImg"),0,0,0,obj.getInt("id")));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        carte_list.modifyCards(shopList,listener);
    }
}