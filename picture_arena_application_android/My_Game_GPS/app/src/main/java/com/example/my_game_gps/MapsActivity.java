package com.example.my_game_gps;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.my_game_gps.databinding.ActivityMapsBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    /* -- Map Activity --*/
    private GoogleMap mMap;
    private ActivityMapsBinding binding;
    private LocationManager locationManager = null;
    private MapsActivity activity;
    private String fournisseur;
    private Location locationPlayer = null;
    private double lat;
    private double lon;
    private Socket mSocket;

    /* -- Player Attribut --*/
    private String token = "";
    private String login = "";
    private String type = "";
    private Integer idPlayer;
    private String username;
    private JSONArray currentTeam;
    private JSONArray equippedItems;
    private int money;
    private String faction="null";

    /* -- View -- */
    private String JSON_STRING = "";
    private Button takePhoto;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (intent != null){
            if (intent.hasExtra("token")){ token = intent.getStringExtra("token"); }
            if (intent.hasExtra("login")){ login = intent.getStringExtra("login"); }
        }
        activity = this;
        String url = "http://54.152.148.26:80/users/"+login;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    idPlayer = response.getInt("userId");
                    username = response.getString("username");
                    faction = response.getString("faction");
                    currentTeam = response.getJSONArray("currentTeam");
                    equippedItems = response.getJSONArray("equippedItems");
                    money = response.getInt("money");
                    type = response.getString("avatar");
                    socketNotif();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
            }
        }){
            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer "+token);
                return params;
            }};
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);

        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        giveGPSRight();
        giveCameraWrite();
        giveCameraRead();
        setContentView(binding.getRoot());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        takePhoto = (Button) findViewById(R.id.button_photo);
        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsActivity.this, CameraActivity.class);
                intent.putExtra("idPlayer", idPlayer);
                intent.putExtra("lon", String.valueOf(lon));
                intent.putExtra("lat", String.valueOf(lat));
                intent.putExtra("username", username);
                startActivity(intent);
            }
        });
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in location and move the camera
        LatLng sydney;
        if(locationPlayer != null) {
            sydney = new LatLng(locationPlayer.getLatitude(),locationPlayer.getLongitude());
        }
        else {
            sydney = new LatLng(-34, 151);
        }
        if(faction.equals("null")) {
            mMap.addMarker(new MarkerOptions().position(sydney).title("player")
                    .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.mipmap.avatar))));
        }
        if(faction.equals("RED") && faction.equals("Woman")) {
            mMap.addMarker(new MarkerOptions().position(sydney).title("player")
                    .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.mipmap.red_avatar_l))));
        }
        else if(faction.equals("RED")) {
            mMap.addMarker(new MarkerOptions().position(sydney).title("player")
                    .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.mipmap.red_avatar_l))));
        }
        if(faction.equals("GREEN")&& type.equals("Woman")) {
            mMap.addMarker(new MarkerOptions().position(sydney).title("player")
                    .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.mipmap.woman_green_r))));
        }
        else if(faction.equals("GREEN")) {
            mMap.addMarker(new MarkerOptions().position(sydney).title("player")
                    .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.mipmap.green_avatar_l))));
        }
        if(faction.equals("BLUE")&& type.equals("Woman")) {
            mMap.addMarker(new MarkerOptions().position(sydney).title("player")
                    .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.mipmap.woman_blue_r_foreground))));
        }
        else if(faction.equals("BLUE")) {
            mMap.addMarker(new MarkerOptions().position(sydney).title("player")
                    .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.mipmap.blue_avatar_l))));
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.animateCamera( CameraUpdateFactory.zoomTo( 15.0f ) );

        // Setting a click event handler for the map
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {
                addMarker();
                haveAFaction();
            }
        });

        this.lat = locationPlayer.getLatitude();
        this.lon = locationPlayer.getLongitude();

        addPoi();

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                String title = marker.getTitle();
                LatLng position = marker.getPosition();
                if(title.equals("player")) {
                    Intent intent = new Intent(MapsActivity.this, Inventory.class);
                    intent.putExtra("idPlayer", idPlayer);
                    intent.putExtra("username", username);
                    intent.putExtra("faction", faction);
                    intent.putExtra("faction", faction);
                    intent.putExtra("type", type);
                    intent.putExtra("money", String.valueOf(money));
                    intent.putExtra("token", token);
                    startActivity(intent);
                }
                if(title.equals("shop")) {
                    Intent intent = new Intent(MapsActivity.this, ShopActivity.class);
                    intent.putExtra("title", title);
                    intent.putExtra("token", token);
                    intent.putExtra("idPlayer", idPlayer);
                    startActivity(intent);
                }
                if(title.equals("arena")) {
                    Intent intent = new Intent(MapsActivity.this, ArenaActivity.class);
                    intent.putExtra("title", title);
                    intent.putExtra("token", token);
                    intent.putExtra("username", username);
                    intent.putExtra("idPlayer", String.valueOf(idPlayer));
                    intent.putExtra("lon", String.valueOf(position.longitude));
                    intent.putExtra("lat", String.valueOf(position.latitude));
                    startActivity(intent);
                }
                return false;
            }
        });
    }

    public void posPlayer(LatLng latLng) {

        // Clears the previously touched position
        mMap.clear();

        // Animating to the touched position
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

        // Placing a marker on the touched position
        mMap.addMarker(new MarkerOptions().position(latLng).title("player")
                .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.mipmap.avatar))));
    }

    public void addPoi() {
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        String URL = "http://54.88.114.0:80/index.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("VOLLEY", response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lon", String.valueOf(lon));
                params.put("lat", String.valueOf(lat));
                return params;
            };
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response.data != null) {
                    JSON_STRING = new String(response.data);
                    responseString = String.valueOf(response.statusCode);
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        requestQueue.add(stringRequest);
    }

    public void addMarker() {
        if(JSON_STRING != "") {
            try {
                JSONObject obj = new JSONObject(JSON_STRING);
                JSONArray poiArray = obj.getJSONArray("poi");
                for (int i = 0; i < poiArray.length(); i++) {
                    JSONObject poiDetail = poiArray.getJSONObject(i);
                    System.out.println(poiDetail.toString());
                    LatLng latlon = new LatLng(poiDetail.getDouble("lat"), poiDetail.getDouble("lon"));
                    if (poiDetail.getString("label").equals("shop")) {
                        mMap.addMarker(new MarkerOptions().position(latlon).title("shop")
                                .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.mipmap.shop))));
                    } else {
                        mMap.addMarker(new MarkerOptions().position(latlon).title("arena")
                                .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.mipmap.arena))));
                    }
                }

                LatLng sydney = new LatLng(locationPlayer.getLatitude(), locationPlayer.getLongitude());
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                mMap.animateCamera( CameraUpdateFactory.zoomTo( 15.0f ) );
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void giveGPSRight() {
        // Case : yes
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            initialiserLocalisation();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 3);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void giveCameraWrite() {
        // Case : yes
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Log.d("Permission","Write to SSD cards");
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 4);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void giveCameraRead() {
        // Case : yes
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Log.i("PERMISSION","WRITE EXTERNAL STORAGE : OK");
        }
        // Case : No
        else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 5);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 3:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission is granted. Continue the action or workflow
                    // in your app.
                    initialiserLocalisation();

                } else {
                    // Explain to the user that the feature is unavailable because
                    // the features requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.
                }
            case 4:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("PERMISSION","WRITE EXTERNAL STORAGE : OK");
                } else {
                    Log.e("PERMISSION","WRITE EXTERNAL STORAGE : NOT OK");
                }
            case 5:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("PERMISSION","WRITE EXTERNAL STORAGE : OK");
                } else {
                    Log.e("PERMISSION","WRITE EXTERNAL STORAGE : NOT OK");
                }
                return;
        }
    }

    // take the user location
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void initialiserLocalisation() {
        if (locationManager == null) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Criteria criteres = new Criteria();

            // la précision  : (ACCURACY_FINE pour une haute précision ou ACCURACY_COARSE pour une moins bonne précision)
            criteres.setAccuracy(Criteria.ACCURACY_FINE);

            // l'altitude
            criteres.setAltitudeRequired(true);

            // la direction
            criteres.setBearingRequired(true);

            // la vitesse
            criteres.setSpeedRequired(true);

            // la consommation d'énergie demandée
            criteres.setCostAllowed(true);
            criteres.setPowerRequirement(Criteria.POWER_HIGH);

            fournisseur = locationManager.getBestProvider(criteres, true);
        }

        if (fournisseur != null) {
            // dernière position connue
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Location localisation = locationManager.getLastKnownLocation(fournisseur);
            Log.d("GPS", localisation.toString());
            String coordonnees = String.format("Latitude : %f - Longitude : %f\n", localisation.getLatitude(), localisation.getLongitude());
            Log.d("GPS", "coordonnees : " + coordonnees);
            String autres = String.format("Vitesse : %f - Altitude : %f - Cap : %f\n", localisation.getSpeed(), localisation.getAltitude(), localisation.getBearing());
            Log.d("GPS", autres);

            if (mMap != null) {
                LatLng latLng = new LatLng(localisation.getLatitude(), localisation.getLongitude());
                posPlayer(latLng);
            } else {
                locationPlayer = localisation;
            }
        }
    }

    public void
    haveAFaction() {
        if(faction.equals("null")) {
            AlertDialog.Builder myPopUp = new AlertDialog.Builder(activity);
            myPopUp.setTitle("choose your faction");
            RequestQueue queue = Volley.newRequestQueue(activity);
            String message = "Hello "+username+", it is a great day ! Today, you will choose your faction !";
            myPopUp.setMessage(message);
            myPopUp.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(MapsActivity.this, ChooseFactionActivity.class);
                    intent.putExtra("idPlayer", idPlayer);
                    intent.putExtra("token", token);
                    intent.putExtra("login", login);
                    startActivityForResult(intent,7);
                }
            });
            myPopUp.show();
        }
    }

    // function to change picture on market on map
    private Bitmap getMarkerBitmapFromView(@DrawableRes int resId) {

        View customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);
        markerImageView.setImageResource(resId);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("faction")) {
                faction = intent.getStringExtra("faction");
            }
        }
        Log.i("New Faction", faction);
    }

    public void socketNotif() {
        try {
            mSocket = IO.socket("http://52.5.145.59:4000/");
            mSocket.connect();
            mSocket.emit("new message", idPlayer);
            mSocket.on("notif", new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    String data = (String) args[0];
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder myPopUp = new AlertDialog.Builder(activity);
                            myPopUp.setTitle("Welcome Back");
                            String message = "Since last time : "+data;
                            myPopUp.setMessage(message);
                            myPopUp.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.i("CHALLENGE","You confirm");
                                }
                            });
                            if(!(data.equals(""))) {
                                myPopUp.show();
                            }
                        }
                    });
                }
            });

        } catch (URISyntaxException e) {Log.e("SOCKET",e.toString());}
    }
}