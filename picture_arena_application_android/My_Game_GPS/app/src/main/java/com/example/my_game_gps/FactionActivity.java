package com.example.my_game_gps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class FactionActivity extends AppCompatActivity {

    /* -- Intent Attribut -- */
    private String token;
    private String faction;

    /* -- Faction Attribut -- */
    private String description;
    private String imgUrl;
    private String bonus;
    private int fSize;
    private int score;

    /* -- View Attribut -- */
    private Button backToInventory;
    private TextView titleText;
    private TextView descriptionText;
    private ImageView imgView;
    private TextView fSizeText;
    private TextView scoreText;
    private TextView advantageText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faction);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("token")) {
                token = intent.getStringExtra("token");
            }
            if (intent.hasExtra("faction")) {
                faction = intent.getStringExtra("faction");
            }
        }

        findTheFaction();
        this.titleText = (TextView) this.findViewById(R.id.myfaction_title);
        this.descriptionText = (TextView) this.findViewById(R.id.myfaction_description);
        this.fSizeText = (TextView) this.findViewById(R.id.myfaction_membre_input);
        this.scoreText = (TextView) this.findViewById(R.id.myfaction_point_input);
        this.advantageText = (TextView) this.findViewById(R.id.myfaction_avantage_input);
        this.imgView = (ImageView) this.findViewById(R.id.myfaction_image);

        this.backToInventory = (Button) this.findViewById(R.id.button_back_to_inventory);
        this.backToInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(1, intent);
                finish();
            }
        });
    }

    public void findTheFaction() {
        String url = "http://54.152.148.26:80/factions/"+faction+"/";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Response: ", response.toString());
                    try {
                        titleText.setText(faction);
                        description = response.getString("description");
                        descriptionText.setText(description);
                        imgUrl = response.getString("imgUrl");
                        Picasso.get().load(imgUrl).into(imgView);
                        bonus = response.getString("bonus");
                        fSize = response.getInt("fSize");
                        fSizeText.setText(String.valueOf(fSize));
                        score = response.getInt("teamScore");
                        scoreText.setText(String.valueOf(score));
                        bonus = response.getString("bonus");
                        advantageText.setText(bonus);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO: Handle error
                }
            }){
                //This is for Headers If You Needed
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json; charset=UTF-8");
                    params.put("Authorization", "Bearer "+token);
                    Log.d("Params",params.toString());
                    return params;
                }};
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);
    }
}