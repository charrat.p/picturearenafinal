package com.example.my_game_gps;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.my_game_gps.databinding.ActivityDeckBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeckActivity extends AppCompatActivity {
    private ActivityDeckBinding binding;
    private CarteListFragement carte_list;
    private DeckActivity activity;
    private int idPlayer;
    private String username;
    private String token;
    private String cardsString;
    private List<Carte> carteList;
    private ListenerCards listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck);
        Intent intent = getIntent();
        if (intent != null) {
            idPlayer = intent.getIntExtra("idPlayer",0);
            if (intent.hasExtra("username")) {
                username = intent.getStringExtra("username");
            }
            if (intent.hasExtra("token")) {
                token = intent.getStringExtra("token");
            }
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_deck);
        this.activity = this;

        showStartup();
        giveTheDeck();

    }

    public void showStartup() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        carte_list = new CarteListFragement();
        transaction.replace(R.id.fragment_container,carte_list);
        transaction.commit();
        carteList = new ArrayList<Carte>();
        carte_list.setList(carteList);

        listener = new ListenerCards(){
            @Override
            public void actionOnACard(Integer id,String name,Integer price, Integer idOwner, Carte card) {
                Log.i("CARD LISTENER", card.toString());
                Intent intent = new Intent(DeckActivity.this, CardActivity.class);
                intent.putExtra("idPlayer", idPlayer);
                intent.putExtra("token", token);
                intent.putExtra("idCard", id);
                intent.putExtra("title", card.getTitle());
                intent.putExtra("urlImage", card.getPicturePath());
                intent.putExtra("attack", card.getAttack());
                intent.putExtra("hp", card.getHp());
                intent.putExtra("speedattack", card.getSpeedattack());
                intent.putExtra("price", card.getPrice());
                startActivityForResult(intent,1);

            }
        };
        carte_list.setListener(listener);
    }

    public void giveTheDeck() {
        Log.i("URL","http://204.236.239.109:80/cardsOwner/"+idPlayer);
        String url = "http://204.236.239.109:80/cardsOwner/"+idPlayer;
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (!response.equals(null)) {
                    Log.e("Your Array Response", response);
                    cardsString = response;
                    displayCards();
                } else {
                    Log.e("Your Array Response", "Data Null");
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error is ", "" + error);
            }
        });
        RequestQueue queue = Volley.newRequestQueue(activity);
        queue.add(request);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void displayCards() {
        cardsString = cardsString.replace("[", "");
        cardsString = cardsString.replace("]", "");
        List<String> listCards = new ArrayList<String>(Arrays.asList(cardsString.split(";")));
        for(int j=0; j < listCards.size();j++) {
            try {
                JSONObject obj = new JSONObject(listCards.get(j));
                Log.i("URL_IMG",obj.getString("urlImg"));
                carteList.add(new Carte(obj.getInt("id"),obj.getInt("owner"),obj.getString("name"),obj.getInt("value"),obj.getString("urlImg"),obj.getInt("attack"),obj.getInt("hp"),obj.getInt("speedattack"),0));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        carte_list.setListener(listener);
        carte_list.modifyCards(carteList,listener);
    }
}