package com.example.my_game_gps;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.my_game_gps.databinding.ActivityArenaBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ArenaActivity extends AppCompatActivity {

    public Button buttonNewChallenge;
    public TextView factionControl;

    /* -- Adapter Attributes --*/
    private ActivityArenaBinding binding;
    private FigtherListFragement fighter_list;
    private ArrayList<Fighter> fighterList;
    private ListenerFighter listener;

    /* -- Player Attributes --*/
    private String lat;
    private String lon;
    private String token;
    private String username;
    private String idPlayer;

    /* -- Fight Attributes --*/
    private String resultFight;
    private String challengeItemsString;

    /* -- Volley Attributes --*/
    private ArenaActivity activity;
    private RequestQueue requestQueue;

    /* -- Arena Attributes --*/
    private String arenaString;
    private int idArena;
    private String factionCtrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        activity = this;
        if (intent != null){
            if (intent.hasExtra("token")){ token = intent.getStringExtra("token"); }
            if (intent.hasExtra("username")){ username = intent.getStringExtra("username"); }
            if (intent.hasExtra("lon")){ lon = intent.getStringExtra("lon"); }
            if (intent.hasExtra("lat")){ lat = intent.getStringExtra("lat"); }
            if (intent.hasExtra("idPlayer")){ idPlayer = intent.getStringExtra("idPlayer"); }
        }
        this.requestQueue = Volley.newRequestQueue(activity);
        setContentView(R.layout.activity_arena);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_arena);
        showStartup();
        publishTheArena();
        recoverTheArena();
    }

    public void showStartup() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        fighter_list = new FigtherListFragement();
        transaction.replace(R.id.fragment_container,fighter_list);
        transaction.commit();

        fighterList = new ArrayList<Fighter>();
        fighter_list.setList(fighterList);

        listener = new ListenerFighter(){
            @Override
            public void acceptADuel(int id, String bet) {
                AlertDialog.Builder myPopUp = new AlertDialog.Builder(activity);
                myPopUp.setTitle("Accept the challenge");
                String message = "Do you want accept the challenge for "+bet+" golds";
                myPopUp.setMessage(message);
                myPopUp.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.i("API REQUEST","Contact API");
                        String URL = "http://52.5.145.59:5000/beginMatch?login="+username+"&idchallenge="+id;
                        Log.i("API URL",URL);
                        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.i("VOLLEY", response);
                                resultFight = response;
                                resultFight(bet);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("VOLLEY", error.toString());
                            }
                        }) {
                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String,String> params = new HashMap<String, String>();
                                params.put("Content-Type","application/x-www-form-urlencoded");
                                params.put("Authorization", "Bearer "+token);
                                Log.d("Params",params.toString());
                                return params;
                            }
                        };
                        requestQueue.add(stringRequest);
                    }
                });
                myPopUp.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        System.out.println("Ok, you give up");
                    }
                });
                myPopUp.show();

            }
        };
        fighter_list.setListener(listener);
    }

    public void publishTheArena() {
        String URL = "http://54.152.148.26:80/challenge/arena/?lat="+lat+"&lon="+lon;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("VOLLEY", response);
                challengeItemsString = response;
                displayShop();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error is ", "" + error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                params.put("Authorization", "Bearer "+token);
                Log.d("Params",params.toString());
                return params;
            }
        };

        requestQueue.add(stringRequest);
    }

    public void recoverTheArena() {
        String URL = "http://54.152.148.26:80/arena/?lat="+lat+"&lon="+lon;
        Log.i("URL",URL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("VOLLEY", response);
                arenaString = response;
                try {
                    Log.i("ARENASTRING",arenaString);
                    JSONObject arena = new JSONObject(arenaString);
                    idArena = arena.getInt("arenaId");
                    factionCtrl = arena.getString("factionCtrl");
                    fighter_list.setBoutton(idPlayer,String.valueOf(idArena),token,factionCtrl);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error is ", "" + error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                params.put("Authorization", "Bearer "+token);
                Log.d("Params",params.toString());
                return params;
            }
        };

        requestQueue.add(stringRequest);
    }

    protected void displayShop() {
        try {
            JSONArray jsonArray = new JSONArray(challengeItemsString);
            Log.i("JSON ARRAY", jsonArray.toString());
            for(int j=0; j < jsonArray.length();j++) {
                JSONObject obj = jsonArray.getJSONObject(j);
                Log.i("SHOP Convert", obj.toString());
                fighterList.add(new Fighter(obj.getInt("id"),obj.getString("username"),"?",obj.getString("faction"),String.valueOf(obj.getInt("bet"))));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        fighter_list.modifyCards(fighterList,listener);
    }

    public void resultFight(String bet) {
        try {
            JSONObject jsonObject = new JSONObject(resultFight);
            Log.i("JSON Object", jsonObject.toString());
            try {
                JSONArray parti = jsonObject.getJSONArray("game");
                Intent intent = new Intent(ArenaActivity.this,DuelActivity.class);
                intent.putExtra("token", token);
                intent.putExtra("username", username);
                intent.putExtra("resultFight", resultFight);
                intent.putExtra("idPlayer", idPlayer);
                intent.putExtra("bet", bet);
                startActivityForResult(intent,7);

            } catch (JSONException jsonException) {
                String cancelReason = jsonObject.getString("game");
                Toast.makeText(activity,cancelReason,Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e)  {
        }
    }
}