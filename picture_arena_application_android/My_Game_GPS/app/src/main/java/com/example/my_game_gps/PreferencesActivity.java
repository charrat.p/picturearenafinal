package com.example.my_game_gps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class PreferencesActivity extends AppCompatActivity {

    private String faction;
    private int idPlayer;
    private ImageView avatarA;
    private ImageView avatarB;
    private Button sendAvatar;
    private Button goBack;
    private String choice;
    private PreferencesActivity activity;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activity = this;
        Intent intent = getIntent();
        if (intent != null) {
            idPlayer = intent.getIntExtra("idPlayer",0);
            if (intent.hasExtra("token")) {
                token = intent.getStringExtra("token");
            }
            if (intent.hasExtra("faction")) {
                faction = intent.getStringExtra("faction");
            }
        }

        setContentView(R.layout.activity_preferences);
        this.avatarA = (ImageView) this.findViewById(R.id.avatar_A);
        this.avatarB = (ImageView) this.findViewById(R.id.avatar_B);

        if(faction.equals("BLUE")) {
            this.avatarA.setImageResource(R.mipmap.blue_avatar_r_foreground);
            this.avatarB.setImageResource(R.mipmap.woman_blue_r_foreground);
        }

        if(faction.equals("RED")) {
            this.avatarA.setImageResource(R.mipmap.red_avatar_r_foreground);
            this.avatarB.setImageResource(R.mipmap.woman_red_l_foreground);
        }
        if(faction.equals("GREEN")) {
            this.avatarA.setImageResource(R.mipmap.green_avatar_r_foreground);
            this.avatarB.setImageResource(R.mipmap.woman_green_r_foreground);
        }

        this.sendAvatar = (Button) this.findViewById(R.id.avatar_submit);
        sendAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestQueue requestQueue = Volley.newRequestQueue(activity);
                String URL = "http://54.152.148.26:80/users/avatar";

                StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("VOLLEY", response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VOLLEY", error.toString());
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Log.i("idPlayer",String.valueOf(idPlayer));
                        Log.i("type",choice);
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("id", String.valueOf(idPlayer));
                        params.put("type", choice);
                        return params;
                    };

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("Content-Type","application/x-www-form-urlencoded");
                        return params;
                    }
                };
                requestQueue.add(stringRequest);
            }
        });
        this.goBack = (Button) this.findViewById(R.id.avatar_end);
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void onRadioButtonClickedToAvatar(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        Log.i("VIEW","CHANGE BODY");

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.avatar_A_button:
                if (checked)
                    choice = "Man";
                break;
            case R.id.avatar_B_button:
                if (checked)
                    choice = "Woman";
                break;
        }
    }
}