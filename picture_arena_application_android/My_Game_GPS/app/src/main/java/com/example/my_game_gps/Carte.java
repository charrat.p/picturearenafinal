package com.example.my_game_gps;

public class Carte {
    private int id;
    private int idOwner;
    private String title;
    private int price;
    private String picturePath;
    private int attack;
    private int hp;
    private int speedattack;
    private int idShopItem;

    public Carte(int id,int idOwner,String title, int price, String picturePath, int attack, int hp, int speedattack,int idShopItem) {
        this.id = id;
        this.idOwner = idOwner;
        this.title = title;
        this.price = price;
        this.picturePath = picturePath;
        this.attack = attack;
        this.hp = hp;
        this.speedattack = speedattack;
        this.idShopItem = idShopItem;
        //TODO add an wincounter
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdOwner() {
        return idOwner;
    }

    public void setIdOwner(int idOwner) {
        this.idOwner = idOwner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getSpeedattack() {
        return speedattack;
    }

    public void setSpeedattack(int speedattack) {
        this.speedattack = speedattack;
    }

    public int getIdShopItem() {
        return idShopItem;
    }

    public void setIdShopItem(int idShopItem) {
        this.idShopItem = idShopItem;
    }

    @Override
    public String toString() {
        return "Carte{" +
                "id=" + id +
                ", idOwner=" + idOwner +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", picturePath='" + picturePath + '\'' +
                ", attack=" + attack +
                ", hp=" + hp +
                ", speedattack=" + speedattack +
                '}';
    }
}
