package com.example.my_game_gps;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import org.json.JSONException;
import org.json.JSONObject;

public class ChooseFactionActivity extends AppCompatActivity {
    private String travel;
    private String fight;
    private String card = "";
    private String animal;
    private String color;
    private String advantage;
    private String login;

    private String token;
    private String faction;
    private Integer idPlayer;

    private Button unknowButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_faction);
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("token")) {
                token = intent.getStringExtra("token");
                Log.i("TOKEN - CHOOSE",token);
            }
            if (intent.hasExtra("login")) {
                login = intent.getStringExtra("login");
            }
            idPlayer = intent.getIntExtra("idPlayer",0);
        }

        unknowButton = (Button) this.findViewById(R.id.button_unknow_faction);
        unknowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ChooseFactionActivity.this, ResultFactionActivity.class);
                intent.putExtra("idPlayer", idPlayer);
                intent.putExtra("token", token);
                intent.putExtra("animal", animal);
                intent.putExtra("color", color);
                intent.putExtra("fight", fight);
                intent.putExtra("bonus", advantage);
                intent.putExtra("travel", travel);
                intent.putExtra("login", login);
                startActivityForResult(intent,7);
            }
        });
    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_yes_figth:
                if (checked)
                    fight = "YES";
                break;
            case R.id.radio_no_figth:
                if (checked)
                    fight = "NO";
                break;
            case R.id.radio_neutral_figth:
                if (checked)
                    fight = "OK";
                break;

            case R.id.radio_yes_travel:
                if (checked)
                    travel = "YES";
                break;
            case R.id.radio_no_travel:
                if (checked)
                    travel = "NO";
                break;
            case R.id.radio_neutral_travel:
                if (checked)
                    travel = "OK";

            case R.id.radio_animal_bull:
                if (checked)
                    animal = "BULL";
                break;
            case R.id.radio_animal_wolf:
                if (checked)
                    animal = "WOLF";
                break;
            case R.id.radio_animal_lion:
                if (checked)
                    animal = "LION";
                break;

            case R.id.radio_color_red:
                if (checked)
                    color = "RED";
                break;
            case R.id.radio_color_blue:
                if (checked)
                    color = "BLUE";
                break;
            case R.id.radio_color_green:
                if (checked)
                    color = "GREEN";
                break;

            case R.id.radio_advantage_card:
                if (checked)
                    advantage = "CARD";
                break;
            case R.id.radio_advantage_money:
                if (checked)
                    advantage = "MONEY";
                break;
            case R.id.radio_advantage_points:
                if (checked)
                    advantage = "POINTS";
                break;

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("Faction")) {
                setResult(7, intent);
                finish();
            }
        }

    }
}