package com.example.my_game_gps;

public interface ListenerFighter {
    void acceptADuel(int id, String bet);
}
