package com.example.my_game_gps;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.my_game_gps.databinding.CartesListFragementBinding;

import java.util.List;

public class CarteListFragement extends Fragment {

    private CartesListFragementBinding binding;
    private CarteListAdapter carteListAdapter;
    private ListenerCards listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,
                R.layout.cartes_list_fragement,container,false);
        binding.carteList.setLayoutManager(new LinearLayoutManager(
                binding.getRoot().getContext()));

        if(carteListAdapter != null) {
            binding.carteList.setAdapter(carteListAdapter);
            carteListAdapter.setListener(this.listener);
        }

        return binding.getRoot();
    }

    public void setList(List<Carte> fakeList) {
        if(binding != null) {
            binding.carteList.setAdapter(new CarteListAdapter(fakeList));
        }
        else carteListAdapter = new CarteListAdapter(fakeList);
    }

    public void setListener(ListenerCards listener) {this.listener = listener;}

    public void modifyCards(List<Carte> fakeList, ListenerCards listenerNew) {
        Log.e("CHANGE DATA","We change the cards");
        carteListAdapter.setCarteList(fakeList);
        carteListAdapter.notifyDataSetChanged();
    }
}