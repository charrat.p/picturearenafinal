package com.example.my_game_gps;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ResultFactionActivity extends AppCompatActivity {

    private EditText knowFaction;
    private Button knowButton;

    private String token;
    private Integer idPlayer;
    private String login;
    private String animal;
    private String color;
    private String fight;
    private String bonus;
    private String travel;

    private ProgressDialog dialog;
    private RequestQueue queue;
    private String factionsString;

    private List<String> allFactions;
    private List<TextView> faction_name;
    private List<TextView> faction_description;
    private List<ImageView> faction_img;
    private TextView faction_tips;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_faction);
        dialog = ProgressDialog.show(this, "Loading", "Calculation of your adapted faction", true);
        knowFaction = (EditText) this.findViewById(R.id.know_faction_input);
        knowButton = (Button) this.findViewById(R.id.button_know_faction);
        allFactions = new ArrayList<String>();
        this.queue = Volley.newRequestQueue(this);
        faction_tips = (TextView) this.findViewById(R.id.result_faction_choose);
        faction_name = new ArrayList<TextView>();
        faction_name.add((TextView) this.findViewById(R.id.faction_1_title));
        faction_name.add((TextView) this.findViewById(R.id.faction_2_title));
        faction_name.add((TextView) this.findViewById(R.id.faction_3_title));

        faction_description = new ArrayList<TextView>();
        faction_description.add((TextView) this.findViewById(R.id.faction_1_descrpition));
        faction_description.add((TextView) this.findViewById(R.id.faction_2_descrpition));
        faction_description.add((TextView) this.findViewById(R.id.faction_3_descrpition));

        faction_img = new ArrayList<ImageView>();
        faction_img.add((ImageView) this.findViewById(R.id.faction_1_picture));
        faction_img.add((ImageView) this.findViewById(R.id.faction_2_picture));
        faction_img.add((ImageView) this.findViewById(R.id.faction_3_picture));

        knowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(knowFaction.getText().toString().equals("name")) && allFactions.contains(knowFaction.getText().toString().toUpperCase())) {
                    String URL = "http://54.152.148.26:80/users/"+login+"/faction/";
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i("VOLLEY", response);
                            Intent intent = new Intent();
                            intent.putExtra("Faction",knowFaction.getText().toString().toUpperCase());
                            setResult(7, intent);
                            finish();
                        }
                    },  new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO: Handle error
                        }
                    }){
                        @Override
                        protected Map<String,String> getParams(){
                            Map<String,String> params = new HashMap<String, String>();
                            params.put("factionname", knowFaction.getText().toString().toUpperCase());
                            Log.d("Params",params.toString());
                            return params;
                        }
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String,String> params = new HashMap<String, String>();
                            params.put("Content-Type","application/x-www-form-urlencoded");
                            params.put("Authorization", "Bearer "+token);
                            Log.d("Params",params.toString());
                            return params;
                        }

                        @Override
                        protected Response<String> parseNetworkResponse(NetworkResponse response) {
                            String responseString = "";
                            if (response != null) {
                                responseString = String.valueOf(response.statusCode);
                                String parsed = new String(response.data);
                                Log.i("FACTION Result ", parsed);
                            }
                            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                        }
                    };
                    queue.add(stringRequest);
                    
                }
            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("token")) {
                token = intent.getStringExtra("token");
                Log.i("TOKEN - RESULT",token);
            }
            if (intent.hasExtra("animal")) {
                animal = intent.getStringExtra("animal");
            }
            if (intent.hasExtra("color")) {
                color = intent.getStringExtra("color");
            }
            if (intent.hasExtra("fight")) {
                fight = intent.getStringExtra("fight");
            }
            if (intent.hasExtra("bonus")) {
                bonus = intent.getStringExtra("bonus");
            }
            if (intent.hasExtra("travel")) {
                travel = intent.getStringExtra("travel");
            }
            if (intent.hasExtra("login")) {
                login = intent.getStringExtra("login");
            }
            idPlayer = intent.getIntExtra("idPlayer",0);
        }
        //advisedFaction();
        recupFaction();
    }

    public void recupFaction() {
        String URL = "http://54.152.148.26:80/factions/";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("VOLLEY", response);
                factionsString = response;
                displayShop();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error is ", "" + error);
            }
        });
        queue.add(stringRequest);
    }

    public void advisedFaction() {
        String URL = "http://54.152.148.26:80/factions/";
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("VOLLEY", response);
                dialog.dismiss();
            }
        },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("animal", animal);
                params.put("color", color);
                params.put("fight", fight);
                params.put("travel", travel);
                params.put("bonus", bonus);
                Log.d("Params",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                params.put("Authorization", "Bearer "+token);
                Log.d("Params",params.toString());
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    String parsed = new String(response.data);
                    Log.i("FACTION TIP", parsed);
                    faction_tips.setText(parsed);
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        queue.add(stringRequest);
    }
    protected void displayShop() {
        try {
            JSONArray jsonArray = new JSONArray(factionsString);
            Log.i("JSON ARRAY", jsonArray.toString());
            for(int j=0; j < jsonArray.length();j++) {
                JSONObject obj = jsonArray.getJSONObject(j);
                faction_name.get(j).setText(obj.getString("fName"));
                faction_description.get(j).setText(obj.getString("description"));
                Picasso.get().load(obj.getString("imgUrl")).into(faction_img.get(j));
                allFactions.add(obj.getString("fName").toUpperCase());
            }
            advisedFaction();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}