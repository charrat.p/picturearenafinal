package com.example.my_game_gps;

import android.content.Context;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.my_game_gps.databinding.CarteItemBinding;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CarteListAdapter extends
        RecyclerView.Adapter<CarteListAdapter.ViewHolder> {
    List<Carte> carteList;
    private ListenerCards listener;

    public CarteListAdapter(List<Carte> fileList) {
        assert fileList != null;
        carteList = fileList;
    }

    public void setCarteList(List<Carte> carteList) {
        this.carteList = carteList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CarteItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.carte_item, parent,false);
        return new ViewHolder(binding);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Carte file = carteList.get(position);
        holder.binding.title.setText(file.getTitle());
        holder.binding.price.setText(Integer.toString(file.getPrice()));
        Log.i("CARDS",file.toString());
        Picasso.get().load(file.getPicturePath()).into(holder.binding.pictureCarte);
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Listener",listener.toString());
                listener.actionOnACard(file.getId(),file.getTitle(),file.getPrice(), file.getIdOwner(),file);
            }
        });
    }

    public void setListener(ListenerCards listener) {  this.listener = listener; }

    @Override
    public int getItemCount() {
        return carteList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private CarteItemBinding binding;
        ViewHolder(CarteItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}