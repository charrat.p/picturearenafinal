package com.example.my_game_gps;

public class Fighter {
    private int id;
    private String name;
    private String level;
    private String faction;
    private String price;

    public Fighter(int id,String name, String level, String faction, String price) {
        this.id = id;
        this.name = name;
        this.level = level;
        this.faction = faction;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getFaction() {
        return faction;
    }

    public void setFaction(String faction) {
        this.faction = faction;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
