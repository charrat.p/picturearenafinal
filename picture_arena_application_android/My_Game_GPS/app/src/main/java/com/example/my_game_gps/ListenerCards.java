package com.example.my_game_gps;

public interface ListenerCards {

    void actionOnACard(Integer id, String name, Integer price, Integer idOwner, Carte card);

}
