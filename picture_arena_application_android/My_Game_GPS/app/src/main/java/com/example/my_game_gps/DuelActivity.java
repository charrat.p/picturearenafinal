package com.example.my_game_gps;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class DuelActivity extends AppCompatActivity {

    /* -- View Attribute --*/
    private Button backToLobby;
    private Button seeTheFight;
    private Button skipTheFight;
    private TextView seeRound;
    private TextView betView;

    private ImageView youFirstCard;
    private ImageView youSecondCard;
    private ImageView youThirdCard;
    private List<ImageView> yourCards;

    private ImageView advFirstCard;
    private ImageView advSecondCard;
    private ImageView advThirdCard;
    private List<ImageView> advCards;

    /* -- Intent Attribute --*/
    private String login;
    private String resultFight;
    private DuelActivity activity;

    private JSONObject result;

    private int idWinner;
    private int idLoose;
    private JSONArray deckLoose;
    private JSONArray deckWinner;
    private String token;
    private String username;
    private String idPlayer;
    private JSONArray deckPlayer;
    private JSONArray deckAdv;
    private JSONArray game;

    private int cptRound=0;
    private int cptUnderRound=0;
    private JSONArray round;
    private JSONObject underRound;
    private JSONObject tmpRound;
    private int cptRoundText=1;
    private String yourCurrentCard = "undefined";
    private String advCurrentCard = "undefined";
    private String bet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        activity = this;
        if (intent != null){
            if (intent.hasExtra("token")){ token = intent.getStringExtra("token"); }
            if (intent.hasExtra("username")){ username = intent.getStringExtra("username"); }
            if (intent.hasExtra("idPlayer")){ idPlayer = intent.getStringExtra("idPlayer"); }
            if (intent.hasExtra("resultFight")){ resultFight = intent.getStringExtra("resultFight"); }
            if (intent.hasExtra("bet")){ bet = intent.getStringExtra("bet"); }
        }

        setContentView(R.layout.activity_duel);
        this.activity = this;
        this.seeRound = (TextView) findViewById(R.id.textView_round);
        this.betView = (TextView) findViewById(R.id.duel_price_input);
        this.betView.setText(bet);
        this.backToLobby = (Button) this.findViewById(R.id.button_return_to_loby);
        this.backToLobby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(1, intent);
                finish();
            }
        });
        this.seeTheFight = (Button) this.findViewById(R.id.button_start_fight);
        this.seeTheFight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playMatch();
            }
        });

        this.skipTheFight = (Button) this.findViewById(R.id.button_skip_the_fight);
        this.skipTheFight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seeOnlyResult();
            }
        });

        this.yourCards = new ArrayList<ImageView>();
        this.youFirstCard = (ImageView) findViewById(R.id.duel_you_first_card);
        this.yourCards.add(this.youFirstCard);
        this.youSecondCard = (ImageView) findViewById(R.id.duel_you_second_card);
        this.yourCards.add(this.youSecondCard);
        this.youThirdCard = (ImageView) findViewById(R.id.duel_you_third_card);
        this.yourCards.add(this.youThirdCard);

        this.advCards = new ArrayList<ImageView>();
        this.advFirstCard = (ImageView) findViewById(R.id.duel_adv_first_card);
        this.advCards.add(this.advFirstCard);
        this.advSecondCard = (ImageView) findViewById(R.id.duel_adv_second_card);
        this.advCards.add(this.advSecondCard);
        this.advThirdCard = (ImageView) findViewById(R.id.duel_adv_third_card);
        this.advCards.add(this.advThirdCard);
        giveCardsPlayer();
    }

    protected void giveCardsPlayer() {
        try {
            JSONObject jsonObject = new JSONObject(resultFight);
            Log.i("JSON Object", jsonObject.toString());
            result = jsonObject.getJSONObject("result");
            idWinner = result.getInt("idWinner");
            idLoose = result.getInt("idLoose");
            game = jsonObject.getJSONArray("game");
            deckLoose = jsonObject.getJSONArray("deckLooser");
            deckWinner = jsonObject.getJSONArray("deckWinner");
            if(idPlayer.equals(String.valueOf(idWinner))) {
                deckPlayer = deckWinner;
                deckAdv = deckLoose;
                for(int i= 0; i<yourCards.size();i++) {
                    JSONObject jsonCards = deckWinner.getJSONObject(i);
                    Picasso.get().load(jsonCards.getString("urlImg")).into(yourCards.get(i));
                    //Picasso.get().load("http://5eti-bucket.s3.amazonaws.com/animal/1_18_VL.png").into(yourCards.get(i));
                }
                for(int j= 0; j<advCards.size();j++) {
                    JSONObject jsonCards = deckLoose.getJSONObject(j);
                    Picasso.get().load(jsonCards.getString("urlImg")).into(advCards.get(j));

                }
            } else {
                deckAdv = deckWinner;
                deckPlayer = deckLoose;
                for(int i= 0; i<advCards.size();i++) {
                    JSONObject jsonCards = deckWinner.getJSONObject(i);
                    Picasso.get().load(jsonCards.getString("urlImg")).into(advCards.get(i));
                    //Picasso.get().load("http://5eti-bucket.s3.amazonaws.com/animal/1_18_VL.png").into(yourCards.get(i));

                }
                for(int j= 0; j<yourCards.size();j++) {
                    JSONObject jsonCards = deckLoose.getJSONObject(j);
                    Picasso.get().load(jsonCards.getString("urlImg")).into(yourCards.get(j));
                    //Picasso.get().load("http://5eti-bucket.s3.amazonaws.com/animal/1_18_VL.png").into(yourCards.get(j));
                }
            }

        } catch (JSONException e)  {e.toString();}
    }

    private void playMatch() {
        try {
            tmpRound = game.getJSONObject(cptRound);
            round = tmpRound.getJSONArray("round"+String.valueOf(cptRoundText));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        new Timer().scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                if(cptUnderRound < round.length()) {
                    try {

                        underRound = round.getJSONObject(cptUnderRound);
                        if(underRound.getString("idAttacker").equals(idPlayer)) {
                            seeRound.setTextColor(Color.GREEN);
                        }
                        else {
                            seeRound.setTextColor(Color.RED);
                        }
                        seeRound.setText(underRound.getString("attacker")+" send "+underRound.getString("damage")+" damages to "+underRound.getString("defencer"));
                        cptUnderRound += 1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if (cptRound < game.length()){
                    cptRound += 1;
                    cptUnderRound=0;
                    cptRoundText+=1;
                    try {
                        tmpRound = game.getJSONObject(cptRound);
                        round = tmpRound.getJSONArray("round"+String.valueOf(cptRoundText));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        if(String.valueOf(idWinner).equals(idPlayer)) {
                            seeRound.setTextColor(Color.YELLOW);
                            seeRound.setText("YOU WIN, GOOD JOB");
                        }
                        else {
                            seeRound.setTextColor(Color.RED);
                            seeRound.setText("YOU LOOSE");
                        }
                    }
                }
            }
        },0,1000);
    }

    public void seeOnlyResult() {
        AlertDialog.Builder myPopUp = new AlertDialog.Builder(activity);
        myPopUp.setTitle("Your result");
        String message;
        Log.i("HELLO","WORLD");
        message = "YOU LOOSE, NEXT TIME MAY BE";
        if(String.valueOf(idWinner).equals(idPlayer)) {
            message = "YOU WIN, PERFECT !";
        }
        myPopUp.setMessage(message);
        myPopUp.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.i("RESULT","See the result");
            }
        });
        myPopUp.show();
    }
}