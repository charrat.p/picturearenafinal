package com.example.my_game_gps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class CreatChallengeActivity extends AppCompatActivity {
    private Button submitChallengeButton;
    private EditText betChallenge;
    private String idArena;
    private CreatChallengeActivity activity;
    private String token;
    private String idPlayer;
    private RequestQueue requestQueue;
    private Button backChallengeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creat_challenge);
        Intent intent = getIntent();
        activity = this;
        this.requestQueue = Volley.newRequestQueue(activity);

        if (intent != null){
            if (intent.hasExtra("token")){ token = intent.getStringExtra("token"); }
            if (intent.hasExtra("idPlayer")){ idPlayer = intent.getStringExtra("idPlayer"); }
            if (intent.hasExtra("idArena")){ idArena = intent.getStringExtra("idArena"); }
        }

        betChallenge = this.findViewById(R.id.price_newchallenge_input);
        backChallengeButton = (Button) this.findViewById(R.id.button_go_back_areana);
        backChallengeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }});
                submitChallengeButton = (Button) this.findViewById(R.id.button_creat_challenge);
        submitChallengeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String URL = "http://54.152.148.26:80/challenge/?arenaId="+idArena+"&userId="+idPlayer+"&bet="+betChallenge.getText().toString();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        if(response.equals("true")) {
                            Toast.makeText(activity,"Challenge send",Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity,"Challenge not accepted (team or money)",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VOLLEY", error.toString());
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("Content-Type","application/x-www-form-urlencoded");
                        params.put("Authorization", "Bearer "+token);
                        Log.d("Params",params.toString());
                        return params;
                    }
                };
                requestQueue.add(stringRequest);
            }
        });
    }
}