package com.project.app.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.project.app.auth.controller.AppAuthProvider;
import com.project.app.auth.controller.AuthService;
import com.project.app.auth.controller.JwtAuthEntryPoint;
import com.project.app.auth.controller.JwtRequestFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	AuthService authService;

	@Autowired
	private JwtAuthEntryPoint jwtAuthenticationEntryPoint;

	@Autowired
	private JwtRequestFilter jwtRequestFilter;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		// use to allow direct login call without hidden value csfr (Cross Site Request
		// Forgery) needed
		http.csrf().disable()
				    .exceptionHandling()
				         .authenticationEntryPoint(jwtAuthenticationEntryPoint)
				    .and()
				    .sessionManagement()
				         .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		http
		.authenticationProvider(getProvider())
				.authorizeRequests().antMatchers("/login").permitAll().anyRequest().authenticated()
				.antMatchers("/arena").permitAll().anyRequest().permitAll()
				.antMatchers("/challenge").permitAll().anyRequest().permitAll()
				.antMatchers("/users//{\\d+}/").permitAll().anyRequest().permitAll()
				.antMatchers("/shop").permitAll().anyRequest().permitAll();

		// Add a filter to validate the tokens with every request
		http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

	}

	@Bean
	public AuthenticationProvider getProvider() {
		AppAuthProvider provider = new AppAuthProvider();
		provider.setUserDetailsService(authService);
		provider.setPasswordEncoder(passwordEncoder);
		return provider;
	}
	
	//Use to expose the current authenticationManager as Bean for other service e.G JwTAuthController
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}