package com.project.app.challenge.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;

import com.project.app.arena.controller.ArenaRepository;
import com.project.app.arena.model.Arena;
import com.project.app.challenge.model.Challenge;
import com.project.app.faction.controller.FactionService;
import com.project.app.user.controller.UserService;
import com.project.app.user.model.User;


import com.project.app.arena.controller.ArenaService;




@Service
public class ChallengeService {
	@Autowired
	ChallengeRepository cRepository;
	
	@Autowired
	ArenaService aService;
	@Autowired
	UserService uService;
	@Autowired
	FactionService fService;
	
	
	public List<Challenge> getAllChallenges() {
		List<Challenge> cL = new ArrayList<>();
		cRepository.findAll().forEach(cL::add);
		return cL; 
	}
	
	public List<Challenge> getAllChallengeInAnArena(Point loc) {
		Arena arena = aService.getArena(loc);
		List<Challenge> listChall = new ArrayList<Challenge>();
		for(int i=0;i<arena.getChallengeIdList().size();i++) {
			listChall.add(getChallenge(arena.getChallengeIdList().get(i)));
		}
		return listChall;
	}
	
	public Challenge getChallenge(int challId) {
		Optional<Challenge> cOpt = cRepository.findById(challId);
		if (cOpt.isPresent()) {
			return cOpt.get();
			}
		System.out.println("Challenge does not exist");
		return null;
	}
	
	public boolean addChallenge(int arenaId, int userId, int bet) {
		List<Challenge> userCL = cRepository.findByUserId(userId);
		User user = uService.getUserID(userId);
		ArrayList<Integer> currentTeamcloned = new ArrayList<Integer>(user.getCurrentTeam());
		
		if(user.getMoney() < bet || currentTeamcloned.size() < 1) {
			return false;
		}
		
		ArrayList<Integer> currentEquipementcloned = new ArrayList<Integer>(user.getEquippedItems());
		Challenge c = new Challenge(arenaId,userId,currentTeamcloned,currentEquipementcloned, bet, user.getUsername() ,user.getFaction());
		cRepository.save(c);
		
		Arena arena = aService.getArenaWithId(arenaId);
		aService.addChallengeId(arena, c.getId());
		return true;
	}
	
	public Boolean finishChallenge(int challId, int idWinner, int idLooser, String factionPlayerDeux) {
		Boolean ret = false;
		Optional<Challenge> cOpt = cRepository.findById(challId);
		if (cOpt.isPresent()) {
			//Init object 
			Challenge c = cOpt.get();
			Arena arena = aService.getArenaWithId(c.getArenaId());
			// Modif money
			int bet = c.getBet();
            putUserPortefeuille(idWinner, bet);
            putUserPortefeuille(idLooser, -1*bet);
            // Faction Point
            int winnerpoint = 100; 
            if(idWinner == c.getUserId()) {
            	if(c.getFaction().equals(arena.getFactionCtrl())) {
            		winnerpoint += 20;
            	}
        		aService.addPointInArena(arena.getArenaId(), winnerpoint, c.getFaction());
            	fService.modifyFactionScore(c.getFaction(),winnerpoint);
            }
            else {
            	if(factionPlayerDeux.equals(arena.getFactionCtrl())) {
            		winnerpoint += 20;	
            	}
            	aService.addPointInArena(arena.getArenaId(), winnerpoint, factionPlayerDeux);
            	fService.modifyFactionScore(factionPlayerDeux,winnerpoint);
            }
            
            ret = removeChallenge(c.getArenaId(),challId);
		}
		return ret;
	}
	
	public boolean removeChallenge(int idArena, int challId) {
		Optional<Challenge> cOpt = cRepository.findById(challId);
		if (cOpt.isPresent()) {
			// TODO strange code, make a remake 
			Challenge c = cOpt.get();
			Arena a = aService.getArenaWithId(idArena);
			aService.removeChallengeId(a.getLocation(), challId);
			cRepository.delete(c);
			return true;
		}
		System.out.println("Challenge no longer exists");
		return false;
	}
	
	private void putUserPortefeuille(int id, int price) {
    	uService.modifyMoneyId(price, id);
    }
}
