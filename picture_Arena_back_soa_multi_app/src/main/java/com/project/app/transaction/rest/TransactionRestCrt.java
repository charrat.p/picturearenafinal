package com.project.app.transaction.rest;

import java.util.List;

import com.project.app.transaction.controller.TransactionService;
import com.project.app.transaction.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionRestCrt {
	@Autowired
	private TransactionService tService;
	
	//retourner toutes les transactions
	@RequestMapping(method=RequestMethod.GET,value="/transactions")
	public List<Transaction> getAllTransactions() {
		System.out.println("return all transactions");
		return tService.getAllTransactions();
	}
	
	//retourner la transaction dont id = ..
	@RequestMapping(method=RequestMethod.GET,value="/transactions/{id1}")
	public Transaction getMsg(@PathVariable String id1) {
		int id = Integer.parseInt(id1);
		Transaction transaction = tService.getTransactionById(id);
		return transaction;
	}
	
	// une nouvelle transaction
	@RequestMapping(method=RequestMethod.POST,value="/transactions/{id}")
	public Boolean addTransaction(@PathVariable String id, @RequestParam String idAcheteur) {
		return tService.closeTransaction(Integer.valueOf(idAcheteur), Integer.valueOf(id));
	}
}