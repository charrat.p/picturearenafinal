package com.project.app.user.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.geo.Point;

import com.project.app.user.controller.UserService;
import com.project.app.user.model.User;
import com.project.app.user.model.UserDto;

@RestController
@RequestMapping("/users")
public class UserRestCrt {
	
	@Autowired
	UserService uService;
	
	@RequestMapping(method=RequestMethod.POST,value="/")
	public boolean addUser(@RequestParam UserDto userDto) {
		return uService.addUser(userDto);
	}

	@RequestMapping(method=RequestMethod.POST,value="/register")
	public boolean addUserFromWeb(@RequestBody UserDto userDto) {
		return uService.addUser(userDto);
	}


	@RequestMapping(method=RequestMethod.GET,value="/{username}")
	public User getUser(@PathVariable String username) {
		return uService.getUserNoPwd(username);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/{username}")
	public boolean getUser(@PathVariable String username, @RequestParam UserDto userDto) {
		return uService.setUser(userDto,username);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/{username}")
	public boolean delUser(@PathVariable String username) {
		return uService.delUser(username);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/")
	public List<User> getAllUser() {
		return uService.getAllUserNoPwd();
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/{username}/money")
	public int getMoney(@PathVariable String username) {
		return uService.getMoney(username);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/{username}/money")
	public boolean modifyMoney(@PathVariable String username, @RequestParam int i) {
		return uService.modifyMoney(i, username);
	}
	
	
	@RequestMapping(method=RequestMethod.GET, value="/{username}/cards")
	public List<Integer> getCardList(@PathVariable String username) {
		return uService.getUserCardList(username);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/{username}/cards")
	public boolean addCardtoList(@PathVariable String username, @RequestParam int cardId) {
		return uService.addCardToUser(username, cardId);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/{username}/cards")
	public boolean removeCardFromList(@PathVariable String username, @RequestParam int cardId) {
		return uService.removeCardFromUser(username, cardId);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{username}/team")
	public List<Integer> getUserTeam(@PathVariable String username) {
		return uService.getUserCurrentTeam(username);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/{username}/team", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean addCardtoTeam(@PathVariable String username, @RequestBody List<TeamDTO> listTeam) {

		List<Integer> cardIdList = new ArrayList<>();
		List<String> cardTypeList = new ArrayList<>();

		for(TeamDTO teamDTO:listTeam){
			cardIdList.add(teamDTO.getCardId());
			cardTypeList.add(teamDTO.getCardType());
			if(!uService.addCardToCurrentTeam(username, cardIdList, cardTypeList)){return false;}
		}

		return true;
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/{username}/team")
	public boolean removeCardFromTeam(@PathVariable String username, @RequestParam int cardId) {
		return uService.removeCardFromTeam(username, cardId);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{username}/team/equip")
	public List<Integer> getUserEquipped(@PathVariable String username){
		return uService.getUserEquippedItems(username);
	}

	@RequestMapping(method=RequestMethod.POST, value="/{username}/team/equip",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean addCardtoEquipped(@PathVariable String username, @RequestBody List<TeamDTO> listTeam) {
		List<Integer> cardIdList = new ArrayList<>();
		List<String> cardTypeList = new ArrayList<>();

		for(TeamDTO teamDTO:listTeam){
			cardIdList.add(teamDTO.getCardId());
			cardTypeList.add(teamDTO.getCardType());
			uService.addUserEquippedItem(username, cardIdList, cardTypeList);
		}
		System.out.println("addCardtoEquipped"+listTeam);
		return true;

	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/{username}/team/equip")
	public boolean removeCardFromEquipped(@PathVariable String username, @RequestParam int cardId, String cardType) {
		return uService.removeUserEquippedItem(username, cardId);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{username}/location")
	public Point getUserLocation(@PathVariable String username) {
		return uService.getUserLocation(username);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/{username}/location")
	public boolean setUserLocation(@PathVariable String username, @RequestParam Point pt) {
		return uService.setUserLocation(username,pt.getX(), pt.getY());
	}	
	
	@RequestMapping(method=RequestMethod.GET, value="/{username}/faction")
	public String getUserFaction(@PathVariable String username) {
		return uService.getUserFaction(username);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/{username}/faction")
	public boolean setUserFaction(@PathVariable String username, @RequestParam String factionname) {
		return uService.setUserFaction(username, factionname);
	}

	@RequestMapping(method=RequestMethod.POST, value="/{username}/time/")
	public Boolean dailyPhoto(@PathVariable String username,@RequestParam String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
		return uService.UseDailyPhotoAttempt(username, dateTime);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{username}/init")
	public void init(@PathVariable String username) {
		uService.initPlayer();
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/avatar")
	public void init(@RequestParam int id,@RequestParam String type) {
		uService.changeAvatar(id, type);
	}


	@RequestMapping(method=RequestMethod.POST, value="/{id}/time/")
	public Boolean dailyIDPhoto(@PathVariable String id,@RequestParam String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
		return uService.UseDailyIDPhotoAttempt(Integer.valueOf(id), dateTime);
	}
	/*
	 * @RequestMapping(method=RequestMethod.POST, value="/{username}/photo") public
	 * boolean UsePicture(@PathVariable String username, @RequestParam String
	 * timestamp) {
	 * 
	 * return uService.UseDailyPhotoAttempt(username, XXXXX) }
	 */

static 	class TeamDTO{
		private Integer cardId;
		private String cardType;

		public TeamDTO(){}

		public Integer getCardId() {
			return cardId;
		}

		public String getCardType() {
			return cardType;
		}

		public void setCardId(Integer cardId) {
			this.cardId = cardId;
		}

		public void setCardType(String cardType) {
			this.cardType = cardType;
		}

	@Override
	public String toString() {
		return "TeamDTO{" +
				"cardId=" + cardId +
				", cardType='" + cardType + '\'' +
				'}';
	}
	}
}
