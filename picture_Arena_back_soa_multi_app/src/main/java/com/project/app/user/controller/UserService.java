package com.project.app.user.controller;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.project.app.faction.controller.FactionRepository;
import com.project.app.faction.model.Faction;
import com.project.app.user.model.User;
import com.project.app.user.model.UserDto;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	@Autowired
	FactionRepository fRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 * Get a diff between two dates
	 * @param date1 the oldest date
	 * @param date2 the newest date
	 * @param timeUnit the unit in which you want the diff
	 * @return the diff value, in the provided unit
	 */
	public static Duration getDateDiff(LocalDateTime from, LocalDateTime to) {
		Duration duration = Duration.between(from, to);
		return duration;
	}
	
	public boolean addUser(UserDto userDto) {
		Optional<User> u = userRepository.findUserByUsername(userDto.getUsername());
		if (!u.isPresent()) {
			User u_new = new User();
			u_new.setUsername(userDto.getUsername());
			u_new.setPassword(passwordEncoder.encode(userDto.getPassword()));
			userRepository.save(u_new);
			return true;
		}
		System.out.println("Username already taken");
		return false;
	}



	public boolean setUser(UserDto userDto, String username) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			u.setUsername(userDto.getUsername());
			u.setPassword(passwordEncoder.encode(userDto.getPassword()));
			userRepository.save(u);
			return true;
		}
		System.out.println("Wrong username");
		return false;
	}

	public boolean delUser(String username) {
		Optional<User> u = userRepository.findUserByUsername(username);
		if (u.isPresent()) {
			userRepository.delete(u.get());
			return true;
		}
		System.out.println("Wrong username");
		return false;
	}

	public User getUser(String username) {
		Optional<User> u = userRepository.findUserByUsername(username);
		if (u.isPresent()) {
			return u.get();
		}
		return null;
	}
	
	public User getUserID(int id) {
		Optional<User> u = userRepository.findById(id);
		if (u.isPresent()) {
			return u.get();
		}
		return null;
	}

	public User getUserNoPwd(String username) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			u.setPassword("*************");
			return u;
		}
		System.out.println("Wrong username");
		return null;
	}

	public List<User> getAllUserNoPwd() {
		// TODO rajouter condition administrateur
		List<User> userList = new ArrayList<>();
		userRepository.findAll().forEach(s -> {
			s.setPassword("*************");
			userList.add(s);
		});
		System.out.println("Wrong username");
		return userList;
	}

	public boolean modifyMoney(int i, String username) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			u.setMoney(u.getMoney() + i);
			userRepository.save(u);
			return true;
		}
		System.out.println("Wrong username");
		return false;
	}
	
	public boolean modifyMoneyId(int i, int id) {
		Optional<User> uOpt = userRepository.findById(id);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			u.setMoney(u.getMoney() + i);
			userRepository.save(u);
			return true;
		}
		System.out.println("Wrong ID");
		return false;
	}

	public int getMoney(String username) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			return u.getMoney();
		}
		System.out.println("Wrong username");
		return -1;
	}
	
	public int getMoneyWithId(int id) {
		Optional<User> uOpt = userRepository.findById(id);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			return u.getMoney();
		}
		System.out.println("Wrong ID");
		return -1;
	}

	public List<Integer> getUserCardList(String username) {
		List<Integer> cL = new ArrayList<>();
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			cL = u.getCardIdList();
			return cL;
		}
		System.out.println("Wrong username");
		return cL;
	}

	public boolean addCardToUser(String username, int cardId) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			List<Integer> cL = u.getCardIdList();
			if (!cL.contains(cardId)) {
				cL.add(cardId);
				u.setCardIdList(cL);
				userRepository.save(u);
				return true;
			}
			System.out.println("User already owns this card");
			return false;
		}
		System.out.println("Wrong username");
		return false;
	}

	public boolean addCardToUserWithId(int id, int cardId) {
		Optional<User> uOpt = userRepository.findById(id);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			List<Integer> cL = u.getCardIdList();
			if (!cL.contains(cardId)) {
				cL.add(cardId);
				u.setCardIdList(cL);
				userRepository.save(u);
				return true;
			}
			System.out.println("User already owns this card");
			return false;
		}
		System.out.println("Wrong ID");
		return false;
	}

	public boolean removeCardFromUser(String username, int cardId) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			List<Integer> cL = u.getCardIdList();
			if (cL.contains(cardId) & !u.getCurrentTeam().contains(cardId) & !u.getEquippedItems().contains(cardId)) {
				cL.remove(cL.indexOf(cardId));
				u.setCardIdList(cL);
				userRepository.save(u);
				return true;
			} else if (u.getCardIdList().contains(cardId) == true) {
				System.out.println("Card is being used");
				return false;
			}
			System.out.println("User does not own this card");
			return false;
		}
		System.out.println("Wrong username");
		return false;
	}
	
	public boolean removeCardFromUserID(int id, int cardId) {
		Optional<User> uOpt = userRepository.findById(id);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			List<Integer> cL = u.getCardIdList();
			if (cL.contains(cardId) & !u.getCurrentTeam().contains(cardId) & !u.getEquippedItems().contains(cardId)) {
				cL.remove(cL.indexOf(cardId));
				u.setCardIdList(cL);
				userRepository.save(u);
				return true;
			} else if (u.getCardIdList().contains(cardId) == true) {
				System.out.println("Card is being used");
				return false;
			}
			System.out.println("User does not own this card");
			return false;
		}
		System.out.println("Wrong ID");
		return false;
	}

	public List<Integer> getUserCurrentTeam(String username) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			List<Integer> cL = u.getCurrentTeam();
			return cL;
		}
		System.out.println("Wrong username");
		return new ArrayList<>();
	}

	public boolean addCardToCurrentTeam(String username, List<Integer> cardIdList,List<String> cardTypeList) {


		Optional<User> uOpt = userRepository.findUserByUsername(username);
		for (String cardType:cardTypeList
			 ) {
			if(   !( cardType.toUpperCase().equals("ANIMAL") || cardType.toUpperCase().equals("HISTORIC") ) ){return false;}
		}
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			u.setCurrentTeam(cardIdList);
			userRepository.save(u);
			return true;
		}
		else{
			System.out.println("User not exist");
			return false;

		}
	}

	public boolean removeCardFromTeam(String username, int cardId) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			List<Integer> cL = u.getCurrentTeam();
			if (cL.contains(cardId) == true) {
				cL.remove(cardId);
				u.setCurrentTeam(cL);
				userRepository.save(u);
				return true;
			}
			System.out.println("User is not using this card");
			return false;
		}
		System.out.println("Wrong username");
		return false;
	}

	public List<Integer> getUserEquippedItems(String username) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			return u.getEquippedItems();
		}
		return new ArrayList<>();
	}

	public boolean addUserEquippedItem(String username, List<Integer> cardIdList,List<String> cardTypeList) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);

		for (String cardType:cardTypeList
		) {
			if(   (! cardType.toUpperCase().equals("EQUIPEMENT"))  &  (!cardType.toUpperCase().equals("EQUIPMENT"))){

				return false;}
		}

		if (uOpt.isPresent()) {
			User u = uOpt.get();

				u.setEquippedItems(cardIdList);
				userRepository.save(u);
				return true;
			}
			else{
			System.out.println("User not exist!");
			return false;
		}


	}

	public boolean removeUserEquippedItem(String username, int cardId) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			List<Integer> iL = u.getEquippedItems();
			if (iL.contains(cardId)) {
				iL.remove(cardId);
				u.setEquippedItems(iL);
				userRepository.save(u);
				System.out.println("Equipment removed from current team.");
				return true;
			}
			System.out.println("Card not equipped");
			return false;
		}
		System.out.println("Wrong username");
		return false;
	}

	public Point getUserLocation(String username) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			Point p = new Point(u.getLatitude(), u.getLongitude());
			return p;
		}
		System.out.println("Wrong username");
		return null;
	}

	public boolean setUserLocation(String username, double latitude, double longitude) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			u.setLatitude(latitude);
			u.setLongitude(longitude);
			userRepository.save(u);
			return true;
		}
		System.out.println("Wrong username");
		return false;
	}

	public String getUserFaction(String username) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			return u.getFaction();
		}
		System.out.println("Wrong username");
		return null;
	}

	public boolean setUserFaction(String username, String factionname) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			Optional<Faction> fOpt = fRepository.findByfName(factionname);
			if (fOpt.isPresent()) {
				Faction f = fOpt.get();
				List<Integer> fL = f.getMembersIdL();
				if (u.getFaction() == null) {
					fL.add(u.getUserId());
					f.setMembersIdL(fL);
					fRepository.save(f);
					u.setFaction(factionname);
					userRepository.save(u);
					return true;
				}
				else {
					Optional<Faction> userfOpt = fRepository.findByfName(u.getFaction());
					Faction prevf = userfOpt.get();
					fL = prevf.getMembersIdL();
					fL.remove(u.getUserId());
					prevf.setMembersIdL(fL);
					fRepository.save(prevf);
					fL = f.getMembersIdL();
					fL.add(u.getUserId());
					f.setMembersIdL(fL);
					fRepository.save(f);
					u.setFaction(factionname);
					userRepository.save(u);
					return true;					
				}
			}
			System.out.println("Wrong faction name");
			return false;
		}
		System.out.println("Wrong username");
		return false;
	}
	
	//getDateDiff(Date date1, Date date2, TimeUnit timeUnit)
	public boolean UseDailyPhotoAttempt(String username, LocalDateTime date) {
		Optional<User> uOpt = userRepository.findUserByUsername(username);
		boolean ret = false;
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			int dailyUses = u.getDailyPhotoCount();
			LocalDateTime prevdate = u.getFirstPictureStamp();
			
			if(prevdate != null) {
				Duration diff = getDateDiff(prevdate, date);	
				if (diff.toSeconds() >= 86400) {
					u.setFirstPictureStamp(date);
					dailyUses = 5;
				}
				
			}
			
			else {
				u.setFirstPictureStamp(date);
				dailyUses = 5;
			}
			
			if (dailyUses > 0) {
				u.setDailyPhotoCount(dailyUses-1);
				ret = true;
			}
			userRepository.save(u);
		}
		return ret;
	}
	
	public boolean UseDailyIDPhotoAttempt(Integer id, LocalDateTime date) {
		Optional<User> uOpt = userRepository.findById(id);
		boolean ret = false;
		if (uOpt.isPresent()) {
			User u = uOpt.get();
			int dailyUses = u.getDailyPhotoCount();
			LocalDateTime prevdate = u.getFirstPictureStamp();
			
			if(prevdate != null) {
				Duration diff = getDateDiff(prevdate, date);	
				if (diff.toSeconds() >= 86400) {
					u.setFirstPictureStamp(date);
					dailyUses = 5;
				}
				
			}
			
			else {
				u.setFirstPictureStamp(date);
				dailyUses = 5;
			}
			
			if (dailyUses > 0) {
				u.setDailyPhotoCount(dailyUses-1);
				ret = true;
			}
			userRepository.save(u);
		}
		return ret;
	}
	
	public void changeAvatar(int id, String type) {
		if(type.equals("Man") ||type.equals("Woman")) {
			User user = getUserID(id);
			user.setAvatar(type);
			userRepository.save(user);
		}
	}

	public void initPlayer() {
		addCardToUserWithId(2, 17);
		addCardToUserWithId(2, 15);
		addCardToUserWithId(2, 12);
		addCardToUserWithId(2, 13);
		addCardToUserWithId(2, 11);
		addCardToUserWithId(2, 10);

		addCardToUserWithId(1, 16);
		addCardToUserWithId(1, 14);
		addCardToUserWithId(1, 13);
		addCardToUserWithId(1, 9);
		addCardToUserWithId(1, 8);

		addCardToCurrentTeam("joe", new ArrayList(Arrays.asList(10, 11, 12)), new ArrayList(Arrays.asList("ANIMAL", "ANIMAL", "ANIMAL")));

		addCardToCurrentTeam("admin", new ArrayList(Arrays.asList(20, 21, 22)), new ArrayList(Arrays.asList("ANIMAL", "ANIMAL", "ANIMAL")));


	}
	
}
