package com.project.app.user.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "USER")

// Extends UserDetails to be checked by authentication
public class User implements Serializable , UserDetails {
    
	
	private static final long serialVersionUID = -3065317807570040617L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;
    private String username;
    private String password;
    private String email;
    private String faction;
    private int money;
    private int dailyPhotoCount;
    private String avatar;
    
    private LocalDateTime firstPictureStamp;
    
    private double latitude;
    private double longitude;
    
    @ElementCollection
    private List<Integer> cardIdList;
    @ElementCollection
    private List<Integer> currentTeam;
    @ElementCollection
    private List<Integer> equippedItems;
    
    
    
    public List<Integer> getEquippedItems() {
		return equippedItems;
	}

	public void setEquippedItems(List<Integer> equippedItems) {
		this.equippedItems = equippedItems;
	}

	public User() {}
    
    public String getFaction() {
		return faction;
	}
	public void setFaction(String faction) {
		this.faction = faction;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
    
    public Integer getUserId() {
        return userId;
    }
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public String getUsername() {
        return username;
    }
    @Override
    public boolean isAccountNonExpired() {
        return false;
    }
    @Override
    public boolean isAccountNonLocked() {
        return false;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }
    @Override
    public boolean isEnabled() {
        return false;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public List<Integer> getCardIdList(){
		return cardIdList;
	}
	public void setCardIdList(List<Integer> cardIdList) {
		this.cardIdList = cardIdList;
	}
	
	public List<Integer> getCurrentTeam(){
		return currentTeam;
	}
	public void setCurrentTeam(List<Integer> currentTeam) {
		this.currentTeam = currentTeam;
	}

	public int getDailyPhotoCount() {
		return dailyPhotoCount;
	}

	public void setDailyPhotoCount(int dailyPhotoCount) {
		this.dailyPhotoCount = dailyPhotoCount;
	}

	public LocalDateTime getFirstPictureStamp() {
		return firstPictureStamp;
	}

	public void setFirstPictureStamp(LocalDateTime firstPictureStamp) {
		this.firstPictureStamp = firstPictureStamp;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

}
