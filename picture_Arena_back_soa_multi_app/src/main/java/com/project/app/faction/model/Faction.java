package com.project.app.faction.model;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "FACTION")
public class Faction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer factionId;
	
	private String fName;
	private String bonus;
	@SuppressWarnings("unused")
	private int fSize;
	private int teamScore;
	private String description;
	private String imgUrl;
	

	@ElementCollection
	private List<Integer> membersIdL;
	
	public Faction() {}
	
	public Faction(String fName) {
		this.fName = fName; 
	}
	
	public int getfSize() {
		return this.membersIdL.size();
	}

	public String getfName() {
		return this.fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}


	public List<Integer> getMembersIdL() {
		return membersIdL;
	}

	public void setMembersIdL(List<Integer> membersIdL) {
		this.membersIdL = membersIdL;
	}
	
	public Integer getId() {
		return this.factionId;
	}

	public String getBonus() {
		return bonus;
	}

	public void setBonus(String bonus) {
		this.bonus = bonus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public int getTeamScore() {
		return teamScore;
	}

	public void setTeamScore(int teamScore) {
		this.teamScore = teamScore;
	}
	
}
