package com.project.app.faction.controller;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.project.app.faction.model.Faction;

public interface FactionRepository extends CrudRepository<Faction, Integer>{

	public Optional<Faction> findByfName(String fName);	
	
}
