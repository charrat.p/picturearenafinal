package com.project.app.faction.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.project.app.faction.model.Faction;

@Service
public class FactionService {

	private final FactionRepository fRepository;
	
	public FactionService(FactionRepository fRepository) {
		this.fRepository = fRepository;
	}
	
	public boolean addFaction(String factionName) {
		Optional<Faction> fn = fRepository.findByfName(factionName);
		if (!fn.isPresent()) {
			Faction f = new Faction(factionName);
			fRepository.save(f);
			System.out.println("New faction:" + f.getfName() + " created");
			return true;
		}
		System.out.println("Faction name already taken");
		return false;
	}
	
	public Faction getFaction(String factionName) {
		Optional<Faction> f = fRepository.findByfName(factionName);
		if (f.isPresent()) {
			return f.get();
		}
		System.out.println("Wrong faction name");
		return null;
	}
	
	public boolean delFaction(String factionName) {
		Optional<Faction> f = fRepository.findByfName(factionName);
		if (f.isPresent()) {
			fRepository.delete(f.get());
			return true;
		}
		System.out.println("Wrong faction name");
		return false;
	}
	
	public List<Integer> getFactionMembers(String factionName) {
		Optional<Faction> f = fRepository.findByfName(factionName);
		if (f.isPresent()) {
			List<Integer> membersL= f.get().getMembersIdL();
			return membersL;
		}		
		System.out.println("Wrong faction name");
		return null;
	}
	
	public int getFactionSize(String factionName) {
		Optional<Faction> f = fRepository.findByfName(factionName);
		if (f.isPresent()) {
			return f.get().getfSize();
		}
		System.out.println("Wrong faction name");
		return -1;
	}
	
	public List<Faction> getAllFactions(){
		List<Faction> fL = new ArrayList<>();
		fRepository.findAll().forEach(fL::add);
		return fL;
	}
	
	public String getFactionBonus(String factionName) {
		Optional<Faction> fname = fRepository.findByfName(factionName);
		if (fname.isPresent()) {
			return fname.get().getBonus();
		}
		return "Wrong faction name";
	}
	
	public boolean setFactionBonus(String factionName, String bonusDesc) {
		Optional<Faction> fname = fRepository.findByfName(factionName);
		if (fname.isPresent()) {
			Faction f = fname.get();
			f.setBonus(bonusDesc);
			fRepository.save(f);
		}
		System.out.println("Wrong faction name");
		return false;
	}
	
	public String getFactionDescription(String factionName) {
		Optional<Faction> fname = fRepository.findByfName(factionName);
		if (fname.isPresent()) {
			Faction f = fname.get();
			return f.getDescription();
		}
		System.out.println("Wrong faction name");
		return null;				
	}
	
	public boolean setFactionDescription(String factionName, String description) {
		Optional<Faction> fname = fRepository.findByfName(factionName);
		if (fname.isPresent()) {
			Faction f = fname.get();
			f.setDescription(description);
			fRepository.save(f);
			return true;
		}
		System.out.println("Wrong faction name");
		return false;		
	}
	
	public int getFactionScore(String factionName) {
		Optional<Faction> fname = fRepository.findByfName(factionName);
		if (fname.isPresent()) {
			Faction f = fname.get();
			return f.getTeamScore();
		}
		System.out.println("Wrong faction name");
		return -1;
	}
	
	public boolean modifyFactionScore(String factionName, int points) {
		Optional<Faction> fname = fRepository.findByfName(factionName);
		if (fname.isPresent()) {
			Faction f = fname.get();
			f.setTeamScore(f.getTeamScore() + points);
			return true;
		}
		System.out.println("Wrong faction name");
		return false;
	}
	
	public String chooseAFaction(String animal,String color,String bonus, String travel, String fight) {
		String ret = "";
		List<Faction> allFactions = getAllFactions();
		int pointRED =0;
		int pointBLUE =0;
		int pointGREEN =0;

		if(animal.equals("BULL")) { pointRED += 1; }
		else if(animal.equals("LION")) { pointBLUE += 1; }
		else if(animal.equals("WOLF")) { pointGREEN += 1; }

		if(color.equals("RED")) { pointRED += 1; }
		else if(color.equals("BLUE")) { pointBLUE += 1; }
		else if(color.equals("GREEN")) { pointGREEN += 1; }
		
		if(bonus.equals("CARD")) { pointRED += 1; }
		else if(bonus.equals("MONEY")) { pointBLUE += 1; }
		else if(bonus.equals("POINTS")) { pointGREEN += 1; }
	
		if(fight.equals("YES")) { pointRED += 1; }
		else if(fight.equals("NO")) { pointBLUE += 1; }
		else if(fight.equals("OK")) { pointGREEN += 1; }

		if(pointRED >= pointBLUE && pointRED >= pointGREEN) {
			ret = "RED";
		}
		if(pointGREEN >= pointBLUE && pointGREEN >= pointRED ) {
			ret = "GREEN";
		}
		if(pointBLUE >= pointGREEN  && pointBLUE  >= pointRED ) {
			ret = "BLUE";
		}
		System.out.println("La faction a été choisie : ");
		System.out.println(ret+pointRED+pointGREEN+pointBLUE);
		return ret;
	}
	
}
