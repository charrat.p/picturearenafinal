package com.project.app.faction.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.app.faction.controller.FactionService;
import com.project.app.faction.model.Faction;

@RestController
@RequestMapping("/factions")
public class FactionRestCrt {

	@Autowired
	FactionService fService;
	
	@RequestMapping(method=RequestMethod.GET,value="/")
	public List<Faction> getFactions(){
		return fService.getAllFactions();
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/")
	public boolean addFaction(@RequestParam String factionName) {
		return fService.addFaction(factionName);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/{factionName}/")
	public Faction getFaction(@PathVariable String factionName) {
		return fService.getFaction(factionName);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/")
	public String chooseFaction(@RequestParam("animal") String animal,@RequestParam("color") String color, @RequestParam("bonus") String bonus,@RequestParam("fight") String fight,@RequestParam("travel") String travel) {
		return fService.chooseAFaction(animal,color,bonus, travel, fight);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/{factionName}/")
	public boolean delFaction(@PathVariable String factionName) {
		return fService.delFaction(factionName);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{factionName}/members/")
	public List<Integer> getFactionMembers(@PathVariable String factionName) {
		return fService.getFactionMembers(factionName);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{factionName}/score/")
	public int getFactionScore(@PathVariable String factionName) {
		return fService.getFactionScore(factionName);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/factionName}/score/")
	public boolean modifyFactionScore(@PathVariable String factionName, @RequestParam int score) {
		return fService.modifyFactionScore(factionName, score);
	}
	
}