package com.project.app.shop.rest;


import com.project.app.shop.controller.ShopService;
import com.project.app.shop.model.ShopItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/shop")
public class ShopRestController {
    @Autowired
    ShopService shopService;

    @RequestMapping(method= RequestMethod.GET,value="/user/{idUser}/")
    public List<ShopItem> getShopItemByUserId(@PathVariable Integer idUser){
        List<ShopItem> items = shopService.getShopItemByUserId(idUser);
        return items;
    }
    
    @RequestMapping(method = RequestMethod.GET,value="/")
    public List<ShopItem> getAllShopItem(){
        return shopService.getAllshopItem();
    }
    
    @RequestMapping(method = RequestMethod.POST,value = "/")
    public void putItemOnShop(@RequestParam("userId") String userId, @RequestParam("cardId") String cardId,@RequestParam("urlImg") String urlImg, @RequestParam("price") String price){
        shopService.addShopItem(new ShopItem(Integer.valueOf(userId),Integer.valueOf(cardId),urlImg,Integer.valueOf(price)));
    }

}

	