package com.project.app.shop.controller;

import com.project.app.shop.model.ShopItem;
import com.project.app.user.controller.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ShopService {
    @Autowired
    ShopItemRepository shopItemRepository;
    @Autowired
    UserService uService;

    public void addShopItem(ShopItem shopItem){
        ShopItem createdItem = shopItemRepository.save(shopItem);
        System.out.println(createdItem);
        //supprimer la carte de l'utilisateur
        deleteUserCarte(shopItem.getUserId(),shopItem.getCardId());
    }

    public ShopItem getShopItemById(Integer id){
        Optional<ShopItem> hOpt = shopItemRepository.findById(id);
        if(hOpt.isPresent()){
        	System.out.println("Shop Item find");
            return  hOpt.get();
        }
        return null;
    }

    public List<ShopItem> getShopItemByUserId(Integer userId){
        List<ShopItem> items = shopItemRepository.findByUserId(userId);
        return items;
    }

    public List<ShopItem> getAllshopItem(){
        List<ShopItem> items = new ArrayList<>();
        for(ShopItem it: shopItemRepository.findAll()){
            items.add(it);
        }
        return items;

    }


    private void deleteUserCarte(int idUser, int idCarte) {
        System.out.println("idUser"+idUser+"+"+idCarte+"-----------------------------------------------------------------------");
        System.out.println("http://localhost:8081/users/"+idUser+"/cartes?id="+idCarte);
        uService.removeCardFromUserID(idUser,idCarte);
    }

    public void deleteShopItem(ShopItem asuppr) {
    	System.out.println("Je supprime l'élement de shopitem");
    	shopItemRepository.delete(asuppr);
    }


}
