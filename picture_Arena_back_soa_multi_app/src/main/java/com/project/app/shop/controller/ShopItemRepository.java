package com.project.app.shop.controller;

import com.project.app.shop.model.ShopItem;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Optional;

public interface ShopItemRepository extends CrudRepository<ShopItem, Integer> {
    Optional<ShopItem> findById(Integer id);
    List<ShopItem> findByUserId(Integer id);
}
