INSERT INTO USER(username, password, latitude, longitude, money, daily_photo_count,faction) VALUES('admin', '$2a$10$dq/6L9dKOq1O.Tlmm16tdeNJ0VBH6cfWE2I6MbRIHX2EQZsRV39f.', 0.0, 0.0, 100000, 5,'RED');
INSERT INTO USER(username, password, latitude, longitude, money, daily_photo_count,faction) VALUES('joe','$2y$10$ySjT676AfjHn4s1TAjlVL.lz9e3i2k0jxkQMb6WDyJzrD7N9rHbFa.',45.771755, 4.831510, 1000, 5,'BLUE');
INSERT INTO FACTION(f_name, bonus, f_size, description, team_score,img_url) VALUES('RED', 'Animals', 0, 'Bonus +5% of drop rate on animal cards',0,'https://philippecharrat.000webhostapp.com/red_faction.png');
INSERT INTO FACTION(f_name, bonus, f_size, description, team_score,img_url) VALUES('BLUE', 'Equipment', 0, 'Bonus +5% of drop rate on equipment cards',0,'https://philippecharrat.000webhostapp.com/blue_faction.png');
INSERT INTO FACTION(f_name, bonus, f_size, description, team_score,img_url) VALUES('GREEN', 'Balanced', 0, 'Bonus +2.5% of drop rate on animal and equipment cards',0,'https://philippecharrat.000webhostapp.com/green_faction.png');

