package com.cpe.springboot.card.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
public class CardReference {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String name;
	private Rarete rarete;
	private CardType type;
	private Integer hp;
	private Integer attack;
	private Integer speedattack;
	private Integer value;
	
	// Constructor 
	public CardReference( String name, Rarete rarete, CardType type, Integer hp, Integer attack,
			Integer speedattack, Integer value) {
		super();
		this.id = id;
		this.name = name;
		this.rarete = rarete;
		this.type = type;
		this.hp = hp;
		this.attack = attack;
		this.speedattack = speedattack;
		this.value = value;
	}

	public CardReference() {
		// TODO Auto-generated constructor stub
	}

	// Getter and Setter 
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Rarete getRarete() {
		return rarete;
	}

	public void setRarete(Rarete rarete) {
		this.rarete = rarete;
	}

	public CardType getType() {
		return type;
	}

	public void setType(CardType type) {
		this.type = type;
	}

	public Integer getHp() {
		return hp;
	}

	public void setHp(Integer hp) {
		this.hp = hp;
	}

	public Integer getAttack() {
		return attack;
	}

	public void setAttack(Integer attack) {
		this.attack = attack;
	}

	public Integer getSpeedattack() {
		return speedattack;
	}

	public void setSpeedattack(Integer speedattack) {
		this.speedattack = speedattack;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "CardReference{" +
				"id=" + id +
				", name='" + name + '\'' +
				", rarete=" + rarete +
				", type=" + type +
				", hp=" + hp +
				", attack=" + attack +
				", speedattack=" + speedattack +
				", value=" + value +
				'}';
	}
}
