package com.cpe.springboot.card.model;

import javax.persistence.*;

@Entity
public class CardModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String urlImg;
	private String urlImgWL;
	private Integer owner;
	private Integer idRef;
	private String name;
	private Rarete rarete;
	private CardType type;
	private Integer hp;
	private Integer attack;
	private Integer speedattack;
	private Integer value;
	
	public CardModel() {

	}
	
	public CardModel( CardReference cRef, Integer owner,String img, String urlImageWL) {
		this.idRef= cRef.getId();
		this.name= cRef.getName();
		this.rarete= cRef.getRarete();
		this.type= cRef.getType();
		this.hp= cRef.getHp();
		this.attack= cRef.getAttack();
		this.speedattack= cRef.getSpeedattack();
		this.value= cRef.getValue();
		this.owner = owner;
		this.urlImg = img;
		this.urlImgWL = urlImageWL;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUrlImg() {
		return urlImg;
	}

	public void setUrlImg(String urlImg) {
		this.urlImg = urlImg;
	}

	public Integer getOwner() {
		return owner;
	}

	public void setOwner(Integer owner) {
		this.owner = owner;
	}

	public Integer getIdRef() {
		return idRef;
	}

	public void setIdRef(Integer idRef) {
		this.idRef = idRef;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Rarete getRarete() {
		return rarete;
	}

	public void setRarete(Rarete rarete) {
		this.rarete = rarete;
	}

	public CardType getType() {
		return type;
	}

	public void setType(CardType type) {
		this.type = type;
	}

	public Integer getHp() {
		return hp;
	}

	public void setHp(Integer hp) {
		this.hp = hp;
	}

	public Integer getAttack() {
		return attack;
	}

	public void setAttack(Integer attack) {
		this.attack = attack;
	}

	public Integer getSpeedattack() {
		return speedattack;
	}

	public void setSpeedattack(Integer speedattack) {
		this.speedattack = speedattack;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
	
	public String getUrlImgWL() {
		return urlImgWL;
	}

	public void setUrlImgWL(String urlImgWL) {
		this.urlImgWL = urlImgWL;
	}

	@Override
	public String toString() {
		return "{\"id\":" + id + ", \"urlImg\":\"" + urlImg + "\", \"owner\":" + owner + ", \"idRef\":" + idRef + ", \"name\":\"" + name
				+ "\", \"rarete\":" + rarete+ ", \"type\":" + type + ", \"hp\":" + hp + ", \"attack\":" + attack + ", \"speedattack\":"
				+ speedattack + ", \"value\":" + value + "}";
	}
}
