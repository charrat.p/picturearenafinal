package com.cpe.springboot.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cpe.springboot.Repository.CardModelRepository;
import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.card.model.CardReference;
import com.cpe.springboot.card.model.CardType;
import com.cpe.springboot.card.model.Rarete;


@Service
public class CardModelService {
	@Autowired
	CardModelRepository cardRepository;
	@Autowired
	CardReferenceService cardRefService;

	public List<CardModel> getAllCardModel() {
		List<CardModel> cardList = new ArrayList<>();
		cardRepository.findAll().forEach(cardList::add);
		return cardList;
	}
	public List<CardModel> getCardByOwner(Integer idOwner) {
		List<CardModel> cardList = new ArrayList<>();
		cardRepository.findByOwner(idOwner).forEach(cardList::add);
		return cardList;
	}

	public int addCard(Integer userId, String label, String urlImage, String urlImageWL) {

		CardReference cardAClone = cardRefService.getCardRefByLabel(label);

		CardModel cardModel = new CardModel(cardAClone, userId, urlImage,urlImageWL);
		cardRepository.save(cardModel);
		return cardModel.getId();

	}
	
	public void updateCardUrl(int id, String url) {
		CardModel cardModel = getCard(id);
		cardModel.setUrlImg(url);
		cardRepository.save(cardModel);

	}
	
	public void updateCardUrlWL(int id, String urlWL) {
		CardModel cardModel = getCard(id);
		cardModel.setUrlImgWL(urlWL);
		cardRepository.save(cardModel);

	}
	
	public void updateCardOwner(int id, int idOwner) {
		CardModel cardModel = getCard(id);
		cardModel.setOwner(idOwner);
		cardRepository.save(cardModel);

	}
	
	public void updateCardRef(CardModel cardModel) {
		cardRepository.save(cardModel);

	}
	public void updateCard(CardModel cardModel) {
		cardRepository.save(cardModel);
	}
	public CardModel getCard(Integer id) {
		Optional<CardModel> cOpt =cardRepository.findById(id);
		if (cOpt.isPresent()) 
		{
			return cOpt.get();
		}
		else 
		{
			return null;
		}
	}
	
	public void deleteCardModel(Integer id) {
		cardRepository.deleteById(id);
	}
	
	public Boolean changeOwner(Integer idOwner, Integer idBuyer, Integer idCard) {
		Boolean ret = false;
		Optional<CardModel> cOpt =cardRepository.findById(idCard);
		if (cOpt.isPresent()) 
		{
			CardModel card = cOpt.get();
			if(card.getOwner() == idOwner) {
				card.setOwner(idBuyer);
				cardRepository.save(card);
				ret = true;
			}
		}
		return ret;
	}
	

	public void initCard() {
		// avec amawon S3 url

		addCard(1,"cat","https://5eti-bucket.s3.amazonaws.com/animal/cat1.png","https://5eti-bucket.s3.amazonaws.com/animal/cat1.png");
		addCard(1,"cat","https://5eti-bucket.s3.amazonaws.com/animal/cat2.png","https://5eti-bucket.s3.amazonaws.com/animal/cat2.png");
		addCard(2,"pig","https://5eti-bucket.s3.amazonaws.com/animal/cat3.png","https://5eti-bucket.s3.amazonaws.com/animal/cat3.png");
		addCard(2,"cow","https://5eti-bucket.s3.amazonaws.com/animal/cat3.png","https://5eti-bucket.s3.amazonaws.com/animal/cat3.png");
		addCard(2,"cat","https://5eti-bucket.s3.amazonaws.com/animal/cat3.png","https://5eti-bucket.s3.amazonaws.com/animal/cat3.png");
		addCard(1,"cow","https://5eti-bucket.s3.amazonaws.com/animal/dog1.png","https://5eti-bucket.s3.amazonaws.com/animal/dog1.png");
		addCard(1,"pig","https://5eti-bucket.s3.amazonaws.com/animal/dog2.png","https://5eti-bucket.s3.amazonaws.com/animal/dog2.png");
		addCard(2,"dog","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");
		addCard(1,"dog","https://5eti-bucket.s3.amazonaws.com/animal/dog4.png","https://5eti-bucket.s3.amazonaws.com/animal/dog4.png");
		addCard(2,"pig","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");
		addCard(2,"cow","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");
		addCard(2,"dog","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");
		addCard(2,"dog","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");

		addCard(2,"knife","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");
		addCard(2,"spoon","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");
		addCard(2,"knife","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");
		addCard(2,"umbrella","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");

		addCard(1,"spoon","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");
		addCard(1,"knife","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");
		addCard(1,"spoon","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");
		addCard(1,"knife","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");
		addCard(1,"umbrella","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");
		addCard(1,"umbrella","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png","https://5eti-bucket.s3.amazonaws.com/animal/dog3.png");


	}
	
	public String cardsOf(Integer id) {
		List<CardModel> allCards = getAllCardModel();
		String ret = "";
		for(int i=0; i < allCards.size();i++) {
			if(allCards.get(i).getOwner() == id) {
				 ret = ret + allCards.get(i).toString() + ";";
			}
		}
		return ret;
	}
	
	public List<CardModel> cardsOfFighter(Integer id) {
		List<CardModel> allCards = getAllCardModel();
		List<CardModel> cardsPlayer = new ArrayList<CardModel>();
		
		for(int i=0; i < allCards.size();i++) {
			if(allCards.get(i).getOwner() == id) {
				cardsPlayer.add(allCards.get(i));
			}
		}
		return cardsPlayer;
	}
	
	public String cardsToDisplay(List<Integer> idCards) {
		String ret = "";
		for(int i=0; i < idCards.size();i++) {
			ret = ret + getCard(idCards.get(i)).toString()+";";
		}
		return ret;
	}
}

