package com.cpe.springboot.card.Controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.cpe.springboot.Service.CardModelService;
import com.cpe.springboot.Service.CardReferenceService;
import com.cpe.springboot.card.model.CardType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.card.model.CardReference;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class CardRestController {

	@Autowired
	CardModelService cardModelService;
	@Autowired
	CardReferenceService cardRefService;
	
	
	@RequestMapping("/cards")
	private List<CardModel> getAllCards() {
		return this.cardModelService.getAllCardModel();
	}
	
	@RequestMapping("/card/{id}")
	private CardModel getCard(@PathVariable String id) {
		return this.cardModelService.getCard(Integer.valueOf(id));
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/card/{id}")
	private void changeCardOwner(@PathVariable String id, @RequestParam int idOwner) {
		this.cardModelService.updateCardOwner(Integer.valueOf(id),idOwner);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/card")
	public int addCard(@RequestParam Integer userId, @RequestParam String label) {
		return cardModelService.addCard(userId,label,"","");
	}	
	
	@RequestMapping(method=RequestMethod.DELETE,value="/card/{id}")
	public void deleteUser(@PathVariable String id) {
		cardModelService.deleteCardModel(Integer.valueOf(id));
	}

	@RequestMapping(method=RequestMethod.POST,value="/addcardbrut")
	public void modifCardWL(@RequestParam Integer cardId, @RequestParam String urlImageWL) {
		cardModelService.updateCardUrlWL(cardId,urlImageWL);
	}

		@RequestMapping(method=RequestMethod.POST,value="/addcardlayer")
		public void modifCard(@RequestParam Integer cardId, @RequestParam String urlImage) {
			cardModelService.updateCardUrl(cardId,urlImage);
	}

	@RequestMapping(method=RequestMethod.GET,value="/cardref/{name}")
	public CardReference sendLabel(@PathVariable String name) {
		return cardRefService.getCardRefByLabel(name);
	}


	@RequestMapping(method=RequestMethod.GET,value="/cardref")
	public List<CardReference> sendAllLabel() {
		return cardRefService.getAllCardRef();
	}




	@RequestMapping(method=RequestMethod.GET,value="/initCardRef")
	public void  initCardB() {
		cardRefService.initCard();
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/initCardModel")
	public void  initCardModel() {
		cardModelService.initCard();
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/cardsOwner/{id}")
	public String  sendCardsListOf(@PathVariable String id) {
		return cardModelService.cardsOf(Integer.valueOf(id));
	}

	@RequestMapping(method=RequestMethod.GET,value="/cardsFight/{id}")
	public List<CardModel>  sendCardsFightListOf(@PathVariable String id) {
		return cardModelService.cardsOfFighter(Integer.valueOf(id));
	}

	@RequestMapping(method=RequestMethod.GET,value="/cardsFight/equip/{id}")
	public List<CardModel>  sendEquipCardsFightById(@PathVariable String id) {
		List<CardModel> all_list =cardModelService.cardsOfFighter(Integer.valueOf(id));
		all_list= all_list.stream().filter(f -> CardType.EQUIPEMENT.compareTo(f.getType()) == 0).collect(Collectors.toList());
		return all_list ;
	}
	@RequestMapping(method=RequestMethod.GET,value="/cardsFight/play/{id}")
	public List<CardModel>  sendPlayCardsFightById(@PathVariable String id) {
		List<CardModel> all_list =cardModelService.cardsOfFighter(Integer.valueOf(id));
		all_list= all_list.stream().filter(f -> CardType.EQUIPEMENT.compareTo(f.getType()) != 0).collect(Collectors.toList());
		return all_list ;
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/cardsToDisplay")
	public String  sendCardsListToAndroid(@RequestParam("cardsToDisplay") String cardsToSell) {
		List<String> cardList = new ArrayList<String>(Arrays.asList(cardsToSell.split(",")));
		List<Integer> cardsConvert = cardList.stream().map(Integer::parseInt).collect(Collectors.toList());
		return cardModelService.cardsToDisplay(cardsConvert);
	}
	
}
