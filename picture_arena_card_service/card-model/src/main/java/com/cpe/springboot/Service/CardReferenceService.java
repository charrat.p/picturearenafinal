package com.cpe.springboot.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cpe.springboot.Repository.CardRefRepository;
import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.card.model.CardReference;
import com.cpe.springboot.card.model.Rarete;
import com.cpe.springboot.card.model.CardType;

@Service
public class CardReferenceService {
	
	@Autowired
	CardRefRepository cardRefRepository;
	

	public List<CardReference> getAllCardRef() 
	{
		List<CardReference> cardRefList = new ArrayList<>();
		cardRefRepository.findAll().forEach(cardRefList::add);
		return cardRefList;
	}

	public CardReference getCardRef(Integer id) 
	{
		Optional<CardReference> cOpt =cardRefRepository.findById(id);
		if (cOpt.isPresent()) 
		{
			return cOpt.get();
		}
		else 
		{
			return null;
		}
	}
	
	public CardReference getCardRefByLabel(String  name) {
		CardReference cardReturn = null;
		List<CardReference> listwithLabel = cardRefRepository.findByName(name);
		if(listwithLabel.size() == 1) {
			cardReturn = listwithLabel.get(0);
		}
		return cardReturn;
	}

	public void addCardRef(CardReference cardRef) 
	{
		cardRefRepository.save(cardRef);
	}

	public void updateCardRef(CardReference cardRef) 
	{
		cardRefRepository.save(cardRef);
	}
	
	public void initCard() {
		CardReference cardInit = new CardReference( "cat", Rarete.SECRETSUPERRARE, CardType.ANIMAL, 10, 3, 1, 1000);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference("dog", Rarete.COMMON, CardType.ANIMAL, 10, 2, 1, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "horse", Rarete.COMMON, CardType.ANIMAL, 8, 4, 1, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "cow", Rarete.COMMON, CardType.ANIMAL, 15, 1, 1, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "sheep", Rarete.COMMON, CardType.ANIMAL, 9, 3, 1, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "zebra", Rarete.COMMON, CardType.ANIMAL, 8, 6, 100, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "bird", Rarete.COMMON, CardType.ANIMAL, 2, 4, 100, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "giraffe", Rarete.COMMON, CardType.ANIMAL, 10, 3, 100, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "bear", Rarete.RARE, CardType.ANIMAL, 10, 10, 100, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "elephant", Rarete.RARE, CardType.ANIMAL, 10, 10, 100, 10);
		cardRefRepository.save(cardInit);

		cardInit = new CardReference( "jeanne", Rarete.RARE, CardType.HISTORIC, 15, 5, 3, 1000);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "orion", Rarete.SECRETSUPERRARE, CardType.HISTORIC, 15, 5, 3, 100000);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "philippeCharrat", Rarete.RARE, CardType.HISTORIC, 15, 5, 3, 1000000);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "kroky", Rarete.RARE, CardType.HISTORIC, 15, 5, 3, 1000000);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "yongbin", Rarete.RARE, CardType.HISTORIC, 15, 5, 3, 1000000);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "fares", Rarete.RARE, CardType.HISTORIC, 15, 5, 3, 1000000);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "laMachine", Rarete.SECRETSUPERRARE, CardType.HISTORIC, 15, 5, 3, 1000000);
		cardRefRepository.save(cardInit);
		
		
		
		cardInit = new CardReference( "umbrella", Rarete.COMMON, CardType.EQUIPEMENT, 0, 2, 100, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "knife", Rarete.COMMON, CardType.EQUIPEMENT, 0, 3, 0, 100);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "spoon", Rarete.COMMON, CardType.EQUIPEMENT, 0, 3, 0, 100);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "tie", Rarete.COMMON, CardType.EQUIPEMENT, 0, 1, 0, 100);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "suitcase", Rarete.COMMON, CardType.EQUIPEMENT, 2, 0, 0, 100);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "book", Rarete.COMMON, CardType.EQUIPEMENT, 1, 0, 0, 100);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "laptop", Rarete.RARE, CardType.EQUIPEMENT, 2, 3, 0, 100);
		cardRefRepository.save(cardInit);
		
	}
	
	/**
	 * Executed after application start
	 */
	@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
		initCard();
	}
}
