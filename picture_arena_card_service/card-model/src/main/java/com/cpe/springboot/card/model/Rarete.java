package com.cpe.springboot.card.model;
public enum Rarete {  
    COMMON, RARE, SUPER, SECRETSUPERRARE
}