module.exports = class PlayerNotif {
    constructor() {
        this.idPlayer=0;
        this.notifStr ="";
    }

    addAnotif(newNotif) {
        this.notifStr += newNotif; 
    }

    sendAllnotif() {
        let allNotifs = this.notifStr;
        this.notifStr = "";
        return allNotifs;
    }
}