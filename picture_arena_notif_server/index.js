const app = require('express')();
const bodyParser = require('body-parser');
const server = require('http').createServer(app);
const io = require('socket.io');
const cors = require ('cors');
const ioServer  = new io.Server(server);

const PlayerNotif = require("./playerNotif");
let notifPlayers = new Map();
let playerNotif;

app.use(cors({ origin: "*" }));
//app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.get('/', function(req, res){
    res.sendfile('public/index.html')
});

  app.post('/notif', function (req, res) {
    if(req.body.idPlayer != undefined && req.body.bet != undefined && req.body.result != undefined && req.body.ennemie != undefined)
	{
        playerNotif = notifPlayers.get(req.body.idPlayer);
		if(playerNotif == undefined) 
		{
            playerNotif = new PlayerNotif();
        }
            console.log("Add a new notif");
            playerNotif.addAnotif(" and you "+req.body.result+" "+req.body.bet+" golds during a challenge against "+req.body.ennemie);
            notifPlayers.set(req.body.idPlayer.toString(),playerNotif)
            console.log(notifPlayers);
    }
	res.send("done");
  });


ioServer.on('connection', function(socket){
	console.log("A Client is connected");
    socket.on('new message', function(data) {
        playerNotif = notifPlayers.get(data.toString());
        console.log(data);
        console.log(notifPlayers);
        if(playerNotif != undefined) {
            let notifs = playerNotif.sendAllnotif();
            console.log(notifs);
            socket.emit("notif",notifs);
        }
    });
});

server.listen(4000);
