CREATE ROLE player_gps
SUPERUSER
LOGIN
PASSWORD 'player_gps';

create database gps_game owner player_gps;

CREATE EXTENSION postgis;

CREATE TABLE gps_game (
  id SERIAL PRIMARY KEY,
  coord GEOMETRY(Point, 4326),
  label VARCHAR(128),
  idOther int
);
